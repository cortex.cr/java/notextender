#!/bin/bash
SCRIPT="$(readlink -f "${0}")"
BASEDIR="$(dirname "${SCRIPT}")"
SCRIPT="$(basename "${SCRIPT}")"

set -euo pipefail

timestamp() {
	date -Ins -u
}

say() {
	echo -e "${@}"
}

fail() {
	say "❌ ${@}" 1>&2
	exit ${EXIT_CODE:-1}
}


# First things first: is netExtender installed?
[ -v NE_HOME ] || NE_HOME="/opt/netExtenderClient"

[ -e "${NE_HOME}" ] || fail "NetExtender is not yet installed - please install it first!"
[ -d "${NE_HOME}" ] || fail "NetExtender is not yet installed - please install it first!"

# Check if the dependency is already installed
say "👉 Getting the location of the local Maven repository..."
[ -v MVN_REPO_DIR ] || MVN_REPO_DIR="$(mvn help:evaluate -Dexpression=settings.localRepository -q -DforceStdout)" || fail "Failed to compute the local Maven repository directory"
[ -n MVN_REPO_DIR ] || fail "Failed to compute the local Maven repository directory"
[ -d "${MVN_REPO_DIR}" ] || fail "The local Maven repository directory doesn't exist at [${MVN_REPO_DIR}]"

say "👉 Getting the version for the DBUS-Java library..."
DBUS_JAVA_VER="$(mvn help:evaluate -Dexpression=dbus.version -q -DforceStdout)" || fail "Failed to compute the version of dbus-java to build"
[ -n "${DBUS_JAVA_VER}" ] || fail "Failed to compute the version of dbus-java to build"

say "👉 Checking if the DBUS-Java dependency (${DBUS_JAVA_VER}) exists..."
if ! mvn dependency:get -Dartifact="com.github.hypfvieh:dbus-java-transport-jnr-unixsocket:${DBUS_JAVA_VER}" -DrepoUrl="file:/${MVN_REPO_DIR}" --offline -q &>/dev/null ; then
	say "❌ Failed to find the DBUS-JAVA library (version ${DBUS_JAVA_VER}) in the local Maven repository, will build it from scratch"
	DBUS_JAVA_DIR="${BASEDIR}/dbus-java"
	if [ ! -d "${DBUS_JAVA_DIR}/.git" ] ; then
		say "👉 Checking out the DBUS-Java Git Repo for compilation"
		mkdir -p "${DBUS_JAVA_DIR}"
		OUT="$(git clone --depth 1 --branch "dbus-java-parent-${DBUS_JAVA_VER}" "https://github.com/hypfvieh/dbus-java.git" "${DBUS_JAVA_DIR}" 2>&1)" || fail "Failed to check out the git repo for DBUS-Java (${DBUS_JAVA_VER}):\n${OUT}"
		say "✅ Checkout complete!"
	fi
	(
		set -euo pipefail
		say "👉 Compiling DBUS-Java (${DBUS_JAVA_VER})..."
		cd "${DBUS_JAVA_DIR}"
		OUT="$(mvn clean install 2>&1)" || fail "Compilation failed:\n${OUT}"
		say "✅ Compilation complete!"
	) || fail "Failed to build the dbus-java:${DBUS_JAVA_VER} dependency"
	say "✅ DBUS-Java (${DBUS_JAVA_VER}) compiled successfully!"
else
	say "✅ DBUS-Java (${DBUS_JAVA_VER}) is already built!"
fi

# First things first: compile
say "👉 Getting the version of notextender..."
NOTEXTENDER_VER="$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)" || fail "Failed to compute the version of notextender to build"
say "👉 Checking if the notextender executable (${NOTEXTENDER_VER}) is already built..."
[ -n "${NOTEXTENDER_VER}" ] || fail "Failed to compute the version of notextender to build"

if ! mvn dependency:get -Dartifact="cr.cortex.notextender:notextender:${NOTEXTENDER_VER}" -DrepoUrl="file:/${MVN_REPO_DIR}" --offline -q &>/dev/null ; then
	say "👉 Compiling notextender (${NOTEXTENDER_VER})..."
	OUT="$(mvn -Prelease clean install 2>&1)" || fail "Compilation failed:\n${OUT}"
	say "✅ notextender (${NOTEXTENDER_VER}) compiled successfully!"
else
	say "✅ notextender (${NOTEXTENDER_VER}) is already built!"
fi

# Clean out the dbus-java crap?
if [ -v DBUS_JAVA_DIR ] ; then
	[ -d "${DBUS_JAVA_DIR}" ] && rm -rf "${DBUS_JAVA_DIR}" || true
fi

# Compile succeeded! Now get the artifact built
TARGET_DIR="${BASEDIR}/target"
SRC="$(find "${TARGET_DIR}" -type f -name 'notextender-*-exe.jar' | head -1)"

[ -n "${SRC}" ] || fail "The executable could not be found in [${TARGET_DIR}]"

# Ok ... so we have a new executable, replace the original one
say "⚠️ Some of the following operations may require escalated privileges"

say "👉 Copying the built executable into [${NE_HOME}]..."
sudo cp -f "${SRC}" "${NE_HOME}" || fail "Failed to copy the source executable from [${SRC}] into [${NE_HOME}]"

SRC="$(basename "${SRC}")"

LIB_DIRS=(/usr/lib /usr/lib64)

NE_JAR="NetExtender.jar"
NE_EXE="${NE_HOME}/${NE_JAR}"

for LIB_DIR in "${LIB_DIRS[@]}" ; do
	# Is the JAR in ${LIB_DIR} as a file?
	NE_LIB="${LIB_DIR}/${NE_JAR}"
	say "👉 Installing the symbolic link into [${LIB_DIR}]..."
	if [ -f "${NE_LIB}" ] && [ ! -h "${NE_LIB}" ] ; then
		sudo rm -f "${NE_EXE}" || fail "Failed to clear the existing [${NE_EXE}] file/symlink"
		sudo mv -f "${NE_LIB}" "${NE_EXE}" || fail "Failed to move the executable from [${NE_LIB}] to [${NE_EXE}]"
	fi
	[ -e "${NE_LIB}" ] || sudo ln -s "${NE_EXE}" "${NE_LIB}" || fail "Failed to create a new symbolic link [${NE_LIB}] -> [${NE_EXE}]"
done

if [ -f "${NE_EXE}" ] && [ ! -h "${NE_EXE}" ] ; then
	say "👉 Backing up the existing executable"
	sudo mv -f "${NE_EXE}" "${NE_EXE}.bak.$(date -u +%Y%m%d-%H%M%SZ)" || fail "Failed to create a backup of the original executable at [${NE_EXE}]"
fi

say "👉 Installing expect..."
sudo apt-get install -y expect

# The link already exists, so remove it and point it to the new file
sudo rm -f "${NE_EXE}" || fail "Failed to remove the existing symbolic link at [${NE_EXE}]"
say "👉 Creating the final symbolic link"
sudo ln -s "${SRC}" "${NE_EXE}" || fail "Failed to create a new symbolic link [${NE_EXE}] -> [${NE_HOME}/${SRC}]"

# Now, install the PPP stuff

SSLVPN="sslvpnroute"
PPP_DIR="/etc/ppp"
say "👉 Installing the PPPD supporting scripts"
for d in up down ; do
	sudo cp -f "${BASEDIR}/bash/${SSLVPN}.${d}" "${PPP_DIR}/ip-${d}.d/${SSLVPN}" || fail "Failed to copy the VPN file [${SSLVPN}.${d}] into [${PPP_DIR}]"
done

say "✅ Installation complete!"
exit 0

$ netExtender  --no-reconnect "access.armedia.com:4433"
NetExtender for Linux - Version 10.2.816
SonicWall
Copyright (c) 2020 SonicWall

User Access Authentication
User: drivera
Password:
Domain: ARMEDIA
Connecting to access.armedia.com:4433...
There is a problem with the site's security certificate.
Warning: self signed certificate in certificate chain
Do you want to proceed? (Y:Yes, N:No, V:View Certificate)v
  Certificate Detail:
  Issued To:
     Common Name (CN):
         access.armedia.com
     Organization (O):

     Organizational Unit (OU):

     Serial Number:
         4A85BFD3AFDBC5B9

  Issued By:
     Common Name (CN):
         Starfield Secure Certificate Authority - G2
     Organization (O):
         Starfield Technologies, Inc.
     Organizational Unit (OU):
         http://certs.starfieldtech.com/repository/

  Validity Period:
     Issued On:
         Mar  8 13:55:51 2022 GMT
     Expires On:
         Apr  9 13:50:49 2023 GMT

  Fingerprints:
     SHA1[2B:FB:18:C9:AC:95:64:20:08:E1:41:89:33:D4:69:25:A5:89:A0:02]


Do you want to proceed? (Y:Yes, N:No, V:View Certificate)n

You choose to not trust server's certificate.

Authentication failure: Connection failed. Check log for details.
NetExtender connection failed.
SSL VPN connection is terminated.
Exiting NetExtender client
package cr.cortex.notextender;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

import org.apache.commons.lang3.concurrent.ConcurrentUtils;
import org.freedesktop.dbus.FileDescriptor;
import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.connections.impl.DBusConnectionBuilder;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusSigHandler;
import org.freedesktop.login1.Manager;
import org.freedesktop.login1.Manager.PrepareForSleep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jnr.posix.POSIX;
import jnr.posix.POSIXFactory;

public class DBusAdapter {

	public static enum InhibitWhat {
		//
		SLEEP, //
		SHUTDOWN,
		//
		;

		private final String value;

		private InhibitWhat() {
			this.value = name().toLowerCase();
		}

		@Override
		public final String toString() {
			return this.value;
		}

		public static InhibitWhat decode(String s) {
			if (s == null) { return null; }
			return InhibitWhat.valueOf(s.toUpperCase());
		}
	}

	public static enum InhibitMode {
		//
		BLOCK, //
		DELAY,
		//
		;

		private final String value;

		private InhibitMode() {
			this.value = name().toLowerCase();
		}

		@Override
		public final String toString() {
			return this.value;
		}

		public static InhibitMode decode(String s) {
			if (s == null) { return null; }
			return InhibitMode.valueOf(s.toUpperCase());
		}
	}

	public static class InhibitorLock implements AutoCloseable {
		private final AtomicBoolean closed = new AtomicBoolean(false);
		private final int[] fd;

		private InhibitorLock(int... fd) {
			this.fd = new int[fd.length];
			System.arraycopy(fd, 0, this.fd, 0, fd.length);
			if (fd.length > 0) {
				DBusAdapter.LOG.debug("Creating a new inhibitor lock with file descriptors # {}",
					Arrays.toString(this.fd));
			}
			this.closed.set(this.fd.length <= 0);
		}

		@Override
		public void close() {
			if (this.closed.compareAndSet(false, true) && (this.fd.length > 0)) {
				for (int fd : this.fd) {
					if (fd <= 0) {
						continue;
					}

					DBusAdapter.LOG.debug("Closing file descriptor # {} for this inhibitor lock", fd);
					DBusAdapter.POSIX.close(fd);
				}
			}
		}

		@Override
		public String toString() {
			return String.format("InhibitorLock [fd=%s, closed=%s]", this.fd, this.closed.get());
		}
	}

	public static interface SignalHandler extends AutoCloseable {
		@Override
		public void close();
	}

	private static final Logger LOG = LoggerFactory.getLogger(DBusAdapter.class);
	private static final SignalHandler NULL_HANDLER = () -> {
	};
	private static final int NULL_FD = -1;
	private static final InhibitorLock NULL_INHIBITOR_LOCK = new InhibitorLock();

	private static final POSIX POSIX = POSIXFactory.getPOSIX();
	private static final ReadWriteLock LOCK = new ReentrantReadWriteLock();
	private static volatile DBusConnection CONNECTION = null;

	private static final String SRC_LOGIN1 = Manager.class.getPackageName();
	private static final String PATH_LOGIN1 = "/" + DBusAdapter.SRC_LOGIN1.replace('.', '/');

	private static final ConcurrentMap<Integer, DBusSigHandler<PrepareForSleep>> HANDLERS = new ConcurrentHashMap<>();
	private static final AtomicReference<Thread> SHUTDOWN_HOOK = new AtomicReference<>(null);

	static {
		DBusAdapter.init();
	}

	public static boolean init() {

		final Lock read = DBusAdapter.LOCK.readLock();
		final Lock write = DBusAdapter.LOCK.writeLock();

		read.lock();
		try {
			DBusConnection c = DBusAdapter.CONNECTION;
			if (c == null) {
				read.unlock();
				write.lock();
				try {
					c = DBusAdapter.CONNECTION;
					if (c == null) {
						DBusAdapter.CONNECTION = DBusConnectionBuilder.forSystemBus().build();
						Thread t = new Thread(DBusAdapter::close, "DBusAdapter Cleanup");
						DBusAdapter.SHUTDOWN_HOOK.set(t);
						Runtime.getRuntime().addShutdownHook(t);
					}
					return (c == null);
				} catch (Exception e) {
					DBusAdapter.LOG.warn("Failed to initialize the DBus connection - can't listen to DBus events", e);
				} finally {
					read.lock();
					write.unlock();
				}
			}
			return false;
		} finally {
			read.unlock();
		}
	}

	public static void close() {
		final Lock write = DBusAdapter.LOCK.writeLock();

		write.lock();
		try {
			DBusAdapter.CONNECTION.close();
		} catch (IOException e) {
			// Ignore it ... we're shutting down
			DBusAdapter.LOG.debug("Exception caught while closing the DBus connection", e);
		} finally {
			Thread t = DBusAdapter.SHUTDOWN_HOOK.getAndSet(null);
			if (t != null) {
				try {
					Runtime.getRuntime().removeShutdownHook(t);
				} catch (IllegalStateException e) {
					// Shutdown is already in progress... ignore the problem
				}
			}
			DBusAdapter.CONNECTION = null;
			write.unlock();
		}
	}

	private static DBusSigHandler<PrepareForSleep> buildHandler(Consumer<Boolean> handler) {
		return (s) -> {
			final boolean starting = s.isStarting();
			DBusAdapter.LOG.debug("Submitting the value [{}] to the handler", starting);
			try {
				handler.accept(starting);
				DBusAdapter.LOG.debug("Handler returned successfully");
			} catch (RuntimeException e) {
				if (DBusAdapter.LOG.isTraceEnabled()) {
					DBusAdapter.LOG.warn("A RuntimeException was caught from the handler", e);
				}
			} finally {
				DBusAdapter.LOG.debug("Signal handled");
			}
		};
	}

	public static final SignalHandler addSuspendHandler(Consumer<Boolean> handler) throws DBusException {
		if (handler == null) { return null; }
		if (DBusAdapter.CONNECTION == null) { return DBusAdapter.NULL_HANDLER; }

		final int key = System.identityHashCode(handler);
		final DBusSigHandler<PrepareForSleep> h = ConcurrentUtils.createIfAbsentUnchecked(DBusAdapter.HANDLERS, key,
			() -> DBusAdapter.buildHandler(handler));

		DBusAdapter.LOCK.readLock().lock();
		try {
			DBusAdapter.CONNECTION.addSigHandler(PrepareForSleep.class, h);
			return () -> {
				try {
					DBusAdapter.removeSuspendHandler(handler);
				} catch (DBusException e) {
					// Ignore this ...
				}
			};
		} finally {
			DBusAdapter.LOCK.readLock().unlock();
		}
	}

	public static final void removeSuspendHandlerUnchecked(Consumer<Boolean> handler) {
		try {
			DBusAdapter.removeSuspendHandler(handler);
		} catch (DBusException e) {
			DBusAdapter.LOG.warn("Failed remove the suspend handler: {}", handler);
		}
	}

	public static final void removeSuspendHandler(Consumer<Boolean> handler) throws DBusException {
		if ((handler == null) || (DBusAdapter.CONNECTION == null)) { return; }
		final int key = System.identityHashCode(handler);
		final DBusSigHandler<PrepareForSleep> h = DBusAdapter.HANDLERS.remove(key);
		if (h != null) {
			DBusAdapter.CONNECTION.removeSigHandler(PrepareForSleep.class, h);
		}
	}

	private static final int getInhibitorFd(InhibitWhat what, String label, String why, InhibitMode mode)
		throws DBusException {
		if (DBusAdapter.CONNECTION == null) { return DBusAdapter.NULL_FD; }
		Manager manager = DBusAdapter.CONNECTION.getExportedObject(DBusAdapter.SRC_LOGIN1, DBusAdapter.PATH_LOGIN1,
			Manager.class);
		if (manager == null) { return DBusAdapter.NULL_FD; }
		final FileDescriptor fd = manager.Inhibit(what.toString(), label, why, mode.toString());
		return fd.getIntFileDescriptor();
	}

	public static final InhibitorLock delaySuspendAndShutdown(String why) {
		try {
			int[] fds = new int[InhibitWhat.values().length];
			int pos = 0;
			for (InhibitWhat what : InhibitWhat.values()) {
				fds[pos++] = DBusAdapter.getInhibitorFd(what, "NotExtender VPN", why, InhibitMode.DELAY);
				DBusAdapter.LOG.debug("Received an inhibitor lock: {}", why);
			}
			return new InhibitorLock(fds);
		} catch (DBusException e) {
			DBusAdapter.LOG.debug("Failed to open an inhibitor lock for: {}", why, e);
			return DBusAdapter.NULL_INHIBITOR_LOCK;
		}
	}

	private DBusAdapter() {
	}

}
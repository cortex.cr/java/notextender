package cr.cortex.notextender;

import java.io.IOException;

import cr.cortex.notextender.view.GUI;

public class Main {

	public static void main(String[] args) throws IOException {
		DBusAdapter.init();
		new GUI().run();
	}
}
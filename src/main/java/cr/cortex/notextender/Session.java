package cr.cortex.notextender;

import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.tools.ChildProcess;
import cr.cortex.notextender.tools.ChildProcess.LinePipedInput;
import cr.cortex.notextender.tools.ChildProcess.LinePipedOutput;
import cr.cortex.notextender.tools.ExeTools;

public class Session {

	private static final Logger LOG = LoggerFactory.getLogger(Session.class);

	// Find pppd ... if we can't find it, we're not viable.
	private static final Path EXE_PPPD = ExeTools.locateExecutable("pppd");

	/**
	 * The location of the "pinentry" executable ...
	 */
	private static final Path EXE_PINENTRY = ExeTools.locateExecutable("pinentry");

	protected static final class Pinentry {

		private static final Pattern LINE = Pattern.compile("^\\s*(\\S+)(?:\\s(.*))?$");

		private static final String CODE_D = "D";
		private static final String CODE_ERR = "ERR";
		private static final String CODE_OK = "OK";

		private final String message;

		private CyclicBarrier barrier = new CyclicBarrier(2);
		private CountDownLatch latch = new CountDownLatch(1);
		private AtomicInteger state = new AtomicInteger(0);
		private AtomicReference<String> command = new AtomicReference<>(null);
		private AtomicReference<String> response = new AtomicReference<>(null);
		private AtomicReference<String> error = new AtomicReference<>(null);

		protected Pinentry(String message) {
			this.message = message;
		}

		private void await(boolean input) {
			try {
				this.barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				throw new RuntimeException("Barrier broken while waiting for" + (input ? "in" : "out") + "put");
			}
		}

		private String sendCommand() {
			await(true);
			String command = this.command.getAndSet(null);
			if (command != null) {
				command += "\n";
			}
			return command;
		}

		private boolean handle(String s) {
			Matcher m = Pinentry.LINE.matcher(s);

			if (!m.matches()) { throw new RuntimeException("Unexpected response format: [" + s + "]"); }

			switch (m.group(1)) {
				case CODE_D:
					if (!this.response.compareAndSet(null, m.group(2))) {
						throw new IllegalStateException("A response was already received");
					}
					break;

				case CODE_OK:
					switch (this.state.getAndIncrement()) {
						case 0:
							this.command.set("SETTITLE NotExtender MFA");
							await(false);
							break;

						case 1:
							this.command.set("SETDESC " + this.message);
							await(false);
							break;

						case 2:
							this.command.set("SETPROMPT Code:");
							await(false);
							break;

						case 3:
							this.command.set("GETPIN");
							await(false);
							break;

						case 4:
							this.latch.countDown();
							await(false);
							return false;

						default:

					}
					break;

				case CODE_ERR:
					this.error.set(m.group(2));
					this.latch.countDown();
					await(false);
					return false;
			}

			// We continue reading output normally
			return true;
		}

		private String getResponse() throws InterruptedException {
			this.latch.await();
			String error = this.error.get();
			if (error != null) { throw new RuntimeException(error); }
			return this.response.get();
		}
	}

	/**
	 * Execute an MFA prompt using the pinentry executable
	 *
	 * @param message
	 * @return the MFA response received, or {@code null} if none was received.
	 */
	protected static char[] MFA_PINENTRY(String message) {
		// Launch an instance of pinentry, and do the interactive thing with it
		Pinentry pinentry = new Pinentry(message);
		ChildProcess p = new ChildProcess(Session.EXE_PINENTRY.toString());

		ChildProcess.Instance instance = null;
		try {
			LinePipedOutput sendCommand = new LinePipedOutput((l) -> pinentry.sendCommand());
			LinePipedInput handle = new LinePipedInput((l, s) -> pinentry.handle(s));
			instance = p.start(sendCommand, handle);
			String response = pinentry.getResponse();
			// If our response was the null string, then the user simply hit "enter" and gave us
			// the empty string as input. Return the empty string.
			return (response != null ? response.toCharArray() : Session.NO_PASSWORD);
		} catch (Exception e) {
			// If we had an error, we return null to indicate that the user canceled
			Session.LOG.error("The pinentry subprocess raised an error", e);
			return null;
		} finally {
			if (instance != null) {
				try {
					instance.destroy();
				} catch (InterruptedException e) {
					// Not really of interest...
				}
			}
		}
	}

	private static final String MFA_SUCCESS = "0";
	private static final Map<String, Function<String, char[]>> MFA_PROVIDERS;
	static {
		Map<String, Function<String, char[]>> mfa = new LinkedHashMap<>();

		// Find the executable "pinentry" in the path first, before adding this
		if (Session.EXE_PINENTRY != null) {
			mfa.put("5", Session::MFA_PINENTRY);
		}

		if (mfa.isEmpty()) {
			MFA_PROVIDERS = Collections.emptyMap();
		} else {
			MFA_PROVIDERS = Collections.unmodifiableMap(mfa);
		}
	}

	private static final Pattern SCRIPT_SCANNER = Pattern.compile("^([^=\\s]+)\\s*=\\s*(.*?);?$");
	private static final String OPTION_DNS_SUFFIXES = "dnsSuffix";

	private static final String USER_AGENT = "SonicWALL NetExtender for Linux 10.2.816";
	private static final char[] NO_PASSWORD = {};
	private static final Supplier<char[]> EMPTY_PASSWORD = () -> Session.NO_PASSWORD;
	private static final String TRUE = Boolean.TRUE.toString();

	private static final String URI_LOGIN = "/cgi-bin/userLogin";
	private static final Header HDR_MESSAGE1 = new BasicHeader("X-NE-message", null);
	private static final Header HDR_MESSAGE2 = new BasicHeader("X-NE-Message", null);
	private static final Header HDR_CACHE_CONTROL = new BasicHeader("Cache-Control", "no-cache");
	private static final Header HDR_PDA = new BasicHeader("X-NE-pda", Session.TRUE);
	private static final Header HDR_RSASTATE = new BasicHeader("X-NE-rsastate", null);
	private static final Header HDR_SESSIONPROMPT = new BasicHeader("X-NE-SESSIONPROMPT", Session.TRUE);
	private static final Header HDR_TF = new BasicHeader("X-NE-tf", Session.TRUE);

	private static final String POST_USERNAME = "username";
	private static final String POST_PASSWORD = "password";
	private static final String POST_DOMAIN = "domain";
	private static final String POST_LOGIN = "login";
	private static final String POST_PSTATE = "pstate";
	private static final String POST_STATE = "state";
	private static final String POST_RADIUS_CHALLENGE = "RADIUSCHALLENGE";
	private static final String POST_RADIUS_REPLY = "radiusReply";

	private static final String URI_VPN = "/cgi-bin/sslvpnclient";

	private static final String URI_LOGOUT = "/cgi-bin/userLogout";

	private final URI server;
	private final String domain;
	private final String username;
	private final Supplier<char[]> password;
	private final Map<String, Function<String, char[]>> mfaProviders;
	private final HttpClientContext httpContext = HttpClientContext.create();
	private final CookieStore cookieStore = new BasicCookieStore();
	private final HttpClient http;
	private final Thread logoutHook;

	public Session(URI server, String domain, String username, Supplier<char[]> password,
		Map<String, Function<String, char[]>> mfaProviders) {
		if (Session.EXE_PPPD == null) {
			throw new RuntimeException("No pppd executable was found in the path - can't use this type of session");
		}

		Objects.requireNonNull(server, "Must provide a server URL to connect to");

		try {
			this.server = new URI(server.getScheme(), null, server.getHost(), server.getPort(), null, null, null);
		} catch (URISyntaxException e) {
			throw new RuntimeException(String.format("Couldn't build the server's base URI from the URI [%s]", server));
		}

		this.domain = domain;
		this.username = (username != null ? username : StringUtils.EMPTY);
		this.password = (password != null ? password : Session.EMPTY_PASSWORD);

		if (mfaProviders != null) {
			if (mfaProviders.isEmpty()) {
				// If there are no MFA providers given, then we want none...
				this.mfaProviders = Collections.emptyMap();
			} else {
				Map<String, Function<String, char[]>> mfa = new LinkedHashMap<>(Session.MFA_PROVIDERS);
				mfaProviders.forEach((k, v) -> {
					if (v != null) {
						mfa.put(k, v);
					} else {
						mfa.remove(k);
					}
				});
				this.mfaProviders = Collections.unmodifiableMap(mfa);
			}
		} else {
			this.mfaProviders = Session.MFA_PROVIDERS;
		}

		int timeout = (int) Duration.of(5, ChronoUnit.SECONDS).toMillis();
		this.httpContext.setCookieStore(this.cookieStore);
		this.httpContext.setRequestConfig(RequestConfig.custom() //
			.setConnectTimeout(timeout) //
			.setConnectionRequestTimeout(timeout) //
			.setSocketTimeout(timeout) //
			.build() //
		);
		this.http = HttpClientBuilder.create() //
			.setUserAgent(Session.USER_AGENT) //
			.build() //
		;
		this.logoutHook = new Thread(this::quietLogout, "HTTP Session Logout Thread");
	}

	private Header[] getMessage(HttpResponse rsp) {
		Header[] message = rsp.getHeaders(Session.HDR_MESSAGE1.getName());
		if ((message == null) || (message.length < 1)) {
			message = rsp.getHeaders(Session.HDR_MESSAGE2.getName());
		}
		if ((message != null) && (message.length < 1)) {
			message = null;
		}
		return message;
	}

	private List<Pair<X509Certificate, String>> getCertificateChain() throws Exception {
		final List<Pair<X509Certificate, String>> certificates = new ArrayList<>();
		SSLContext ctx = SSLContext.getInstance("SSL");
		ctx.init(null, new TrustManager[] {
			new X509TrustManager() {

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					Principal lastIssuer = null;
					for (X509Certificate cert : chain) {
						// Check to make sure chain is okay
						Principal subject = cert.getSubjectDN();
						if ((lastIssuer != null) && !subject.equals(lastIssuer)) {
							throw new CertificateException(String.format(
								"Certificate chain is incorrect - the previous certificate's issuer doesn't match this certificate's subject!%nexpected: %s%n  but found: %s",
								lastIssuer, subject));
						}
						certificates.add(Pair.of(cert, DigestUtils.sha1Hex(cert.getEncoded()).toLowerCase()));
						lastIssuer = cert.getIssuerDN();
					}
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

			}
		}, null);

		try (Socket s = ctx.getSocketFactory().createSocket(InetAddress.getByName(this.server.getHost()),
			this.server.getPort())) {
			// We have to attempt communication in order for the SSL/TLS negotiation to occur
			try (OutputStream out = s.getOutputStream()) {
				out.write(0);
			}
		}
		return certificates;
	}

	private void login(Predicate<List<Pair<X509Certificate, String>>> certificateCheck,
		Collection<NameValuePair> extraData) throws Exception {

		// First things first - do we accept the certificate? We allow the certificate check to be
		// optional because we do recursive calls, and it only needs to happen on the first call
		if ((certificateCheck != null) && !certificateCheck.test(getCertificateChain())) {
			throw new Exception("Certificate chain was not accepted");
		}

		HttpPost post = new HttpPost(this.server.resolve(Session.URI_LOGIN));
		post.addHeader(Session.HDR_SESSIONPROMPT);
		post.addHeader(Session.HDR_PDA);
		post.addHeader(Session.HDR_CACHE_CONTROL);
		List<NameValuePair> params = new ArrayList<>();
		char[] password = this.password.get();
		try {
			params.add(new BasicNameValuePair(Session.POST_USERNAME, this.username));
			params.add(new BasicNameValuePair(Session.POST_PASSWORD, new String(password)));
			params.add(new BasicNameValuePair(Session.POST_DOMAIN, this.domain));
			params.add(new BasicNameValuePair(Session.POST_LOGIN, Session.TRUE));
			if (extraData != null) {
				params.addAll(extraData);
			}

			post.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse rsp = this.http.execute(post, this.httpContext);

			// What did the server say?
			Header[] message = getMessage(rsp);

			// Now ... were we asked for MFA?
			Header[] mfa = rsp.getHeaders(Session.HDR_TF.getName());
			if ((mfa != null) && (mfa.length > 0)) {

				// Sanitize the MFA protocol we were asked to apply
				String type = mfa[0].getValue();
				if (type == null) {
					type = StringUtils.EMPTY; // for null-safety
				}

				if (!Session.MFA_SUCCESS.equals(type)) {
					// Find our protocol reader
					Function<String, char[]> mfaReader = this.mfaProviders.get(type);
					if (mfaReader == null) {
						throw new Exception("Unsupported MFA protocol requested: [" + type + "]");
					}

					// We have our MFA protocol reader, so use it
					char[] mfaResponse = mfaReader.apply(message[0].getValue());
					String mfaResponseStr = (mfaResponse != null ? new String(mfaResponse) : StringUtils.EMPTY);

					// Get the RSA State
					Header[] rsaState = rsp.getHeaders(Session.HDR_RSASTATE.getName());
					if ((rsaState == null) || (rsaState.length < 1)) {
						throw new Exception("No RSA state header was provided to continue with MFA");
					}

					// Prepare the recursion
					Collection<NameValuePair> mfaInfo = new LinkedList<>();
					mfaInfo.add(new BasicNameValuePair(Session.POST_PSTATE, rsaState[0].getValue()));
					mfaInfo.add(new BasicNameValuePair(Session.POST_STATE, Session.POST_RADIUS_CHALLENGE));
					mfaInfo.add(new BasicNameValuePair(Session.POST_RADIUS_REPLY, mfaResponseStr));
					// The certificate check only happens on the first connection.
					login(null, mfaInfo);
					return;
				}
			}

			// Ok so we got no MFA request. Were we given an error message?
			if (message != null) {
				// Server had a message - just raise it as an exception.
				throw new Exception(String.format("Login operation failed: %s", message[0].getValue()));
			}

			// No errors ... so we're good to go!! Register the exit logger
			Runtime.getRuntime().addShutdownHook(this.logoutHook);
		} finally {
			Arrays.fill(password, '\0');
		}
	}

	public void login(Predicate<List<Pair<X509Certificate, String>>> certificateCheck) throws Exception {
		login(certificateCheck, null);
	}

	public void run(Consumer<String> messages) throws Exception {
		if (messages == null) {
			messages = (m) -> {
			};
		}

		// 1) Get the HTML with the routing/etc info
		HttpGet vpnRequest = new HttpGet( //
			new URIBuilder(this.server.resolve(Session.URI_VPN)) //
				.addParameter("launchplatform", "mac") //
				.addParameter("neProto", "3") //
				// TODO: This should be configurable
				.addParameter("supportipv6", "yes") //
				.build() //
		);

		HttpResponse rsp = this.http.execute(vpnRequest, this.httpContext);
		Header[] message = getMessage(rsp);
		if (message != null) {
			throw new Exception(String.format("VPN Connection operation failed: %s", message[0].getValue()));
		}

		HttpEntity entity = rsp.getEntity();
		Header encoding = entity.getContentEncoding();

		// If the encoding isn't given ... what then?
		String charset = (encoding != null ? encoding.getValue() : StandardCharsets.UTF_8.displayName());
		final Document doc;
		try (InputStream in = entity.getContent()) {
			doc = Jsoup.parse(in, charset, vpnRequest.getURI().toString());
		}

		Elements scripts = doc.select("script");
		if (scripts.isEmpty()) {
			throw new Exception("The VPN response did not contain the <script> section we were looking for");
		}

		/*
		 *   <script>
		 *     function neLauncherInit(){
		 *     NELaunchX1.userName = "drivera";
		 *     NELaunchX1.domainName = "LocalDomain";
		 *     SessionId = QkMO6MFoLUdjNiCNLyakRw==;
		 *     Route = 192.168.30.0/255.255.255.0
		 *     Route = 192.168.20.0/255.255.255.0
		 *     Route = 0.0.0.0/255.255.255.255
		 *     Route = 10.254.0.0/255.255.248.0
		 *     Route = 172.31.25.0/255.255.255.192
		 *     Route = 10.20.0.0/255.254.0.0
		 *     Route = 10.60.0.0/255.255.0.0
		 *     Route = 10.40.0.0/255.255.0.0
		 *     Route = 10.50.10.0/255.255.254.0
		 *     dns1 = 10.60.10.98
		 *     dns2 = 10.60.10.99
		 *     ipv6Support = no
		 *     dnsSuffix = armedia.com
		 *     dnsSuffixes =armedia.com
		 *     dnsSuffixes =appdev.armedia.com
		 *     pppFrameEncoded = 0;
		 *     PppPref = async
		 *     TunnelAllMode = 0;
		 *     ExitAfterDisconnect = 0;
		 *     UninstallAfterExit = 0;
		 *     NoProfileCreate = 0;
		 *     AllowSavePassword = 0;
		 *     AllowSaveUser = 1;
		 *     AllowSavePasswordInKeychain = 1
		 *     AllowSavePasswordInKeystore = 1
		 *     ClientIPLower = "10.254.0.0";
		 *     ClientIPHigh = "10.254.7.255";
		 *     }
		 *   </script>
		 */
		Element script = scripts.first();
		Map<String, Set<String>> options = new LinkedHashMap<>();
		try (LineNumberReader lnr = new LineNumberReader(new StringReader(script.html()))) {
			while (true) {
				String line = lnr.readLine();
				if (line == null) {
					break;
				}

				Matcher m = Session.SCRIPT_SCANNER.matcher(line);
				if (!m.matches()) {
					// Not something we're interested in, or know what to do with
					continue;
				}

				String key = m.group(1);
				String value = m.group(2);
				if (value.startsWith("\"") && value.endsWith("\"")) {
					// Strip out the quotes ...
					value = value.substring(1, value.length() - 1);
				}

				// Special cases ...
				if (key.matches("^dns\\d+$")) {
					key = "dns";
				} else if (key.startsWith(Session.OPTION_DNS_SUFFIXES)) {
					key = Session.OPTION_DNS_SUFFIXES;
				}

				// If the key is dns?, then shorten it to "dns"

				options.computeIfAbsent(key, (k) -> new LinkedHashSet<>()).add(value);
			}
		}

		// TODO: We have the options, now we can configure PPPD

	}

	public void logout() {
		try {
			this.http.execute(new HttpGet(this.server.resolve(Session.URI_LOGOUT)), this.httpContext);
		} catch (Exception e) {
			// We don't really care ...
		} finally {
			Runtime.getRuntime().removeShutdownHook(this.logoutHook);
		}
	}

	public void quietLogout() {
		try {
			logout();
		} catch (Exception e) {
			throw new RuntimeException("Exception caught while logging out", e);
		}
	}
}
package cr.cortex.notextender.model.config;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration implements Preferences {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final Pattern URL_PATTERN = Pattern.compile("^(.*?)(?::([1-9][0-9]*))?$");
	private static final Charset PASSWORD_ENCODING = StandardCharsets.UTF_16BE;
	private static final Charset STORAGE_ENCODING = StandardCharsets.ISO_8859_1;
	private static final CharBuffer EMPTY_PASSWORD = CharBuffer.allocate(0).asReadOnlyBuffer();
	private static final String[] PAD = {
		"", "=", "==", "==="
	};

	private static final String PREF_EXECUTABLE = "executable";
	private static final String PREF_TIMEOUT = "timeout";
	private static final String PREF_CIPHER = "cipher";
	private static final String PREF_NO_ROUTE = "noRoute";
	private static final String PREF_MTU = "mtu";
	private static final String PREF_PPP_SYNCHRONICITY = "pppSync";
	private static final String PREF_AUTO_RECONNECT = "autoReconnect";
	private static final String PREF_DNS_PREFERENCE = "dnsPreference";
	private static final String PREF_SAVE_PASSWORDS = "savePasswords";

	public static boolean isValidServerUrl(String url) {
		if (url == null) { return false; }
		String s = url.toString();
		Matcher m = Configuration.URL_PATTERN.matcher(s);
		if (!m.matches()) { return false; }

		String host = m.group(1);
		if (StringUtils.isBlank(host)) { return false; }
		if (!InetAddressValidator.getInstance().isValid(host) && !DomainValidator.getInstance().isValid(host)) {
			return false;
		}

		String port = m.group(2);
		if ((port != null) && (Integer.parseInt(port) > 65535)) { return false; }
		return true;
	}

	public static String encodePassword(CharBuffer pass) {
		if (StringUtils.isEmpty(pass)) { return "x"; }
		ByteBuffer encoded = Base64.getEncoder().encode(Configuration.PASSWORD_ENCODING.encode(pass));
		CharBuffer str = Configuration.STORAGE_ENCODING.decode(encoded);
		return StringUtils.reverse(str.toString()).replaceAll("=+", "");
	}

	public static CharBuffer decodePassword(String pass) {
		if (StringUtils.isEmpty(pass) || StringUtils.equalsIgnoreCase("x", pass)) {
			return Configuration.EMPTY_PASSWORD;
		}
		pass = StringUtils.reverse(pass);

		int missing = 4 - (pass.length() % 4);
		if (missing < 4) {
			pass = pass + Configuration.PAD[missing];
		}
		ByteBuffer encoded = Configuration.STORAGE_ENCODING.encode(CharBuffer.wrap(pass));
		return Configuration.PASSWORD_ENCODING.decode(Base64.getDecoder().decode(encoded));
	}

	public final Collection<ConnectionProfile> profiles = new ArrayList<>();

	private String executable;
	private Integer timeout;
	private String cipher;
	private Boolean noRoute;
	private Integer mtu;
	private PPPSynchronicity pppAsyncMode;
	private AutoReconnectMode autoReconnect;
	private DNSPreference dnsMode;
	private Boolean savePasswords;

	@Override
	public String getExecutable() {
		return this.executable;
	}

	@Override
	public Configuration setExecutable(String executable) {
		this.executable = executable;
		return this;
	}

	@Override
	public Integer getTimeout() {
		return this.timeout;
	}

	@Override
	public Configuration setTimeout(Integer timeout) {
		this.timeout = nullIfDefault(timeout, Defaults.DEFAULT_TIMEOUT);
		return this;
	}

	@Override
	public String getCipher() {
		return this.cipher;
	}

	@Override
	public Configuration setCipher(String cipher) {
		this.cipher = nullIfDefault(cipher, Defaults.DEFAULT_CIPHER);
		return this;
	}

	@Override
	public Boolean getNoRoute() {
		return this.noRoute;
	}

	@Override
	public Configuration setNoRoute(Boolean noRoute) {
		this.noRoute = nullIfDefault(noRoute, Defaults.DEFAULT_NO_ROUTE);
		return this;
	}

	@Override
	public Integer getMtu() {
		return this.mtu;
	}

	@Override
	public Configuration setMtu(Integer mtu) {
		this.mtu = nullIfDefault(mtu, Defaults.DEFAULT_MTU);
		return this;
	}

	@Override
	public PPPSynchronicity getPppSynchronicity() {
		return this.pppAsyncMode;
	}

	@Override
	public Configuration setPppSynchronicity(PPPSynchronicity pppSynchronicity) {
		this.pppAsyncMode = nullIfDefault(pppSynchronicity, Defaults.DEFAULT_PPP_SYNCHRONICITY);
		return this;
	}

	@Override
	public AutoReconnectMode getAutoReconnect() {
		return this.autoReconnect;
	}

	@Override
	public Configuration setAutoReconnect(AutoReconnectMode autoReconnect) {
		this.autoReconnect = nullIfDefault(autoReconnect, Defaults.DEFAULT_AUTO_RECONNECT);
		return this;
	}

	@Override
	public DNSPreference getDnsPreference() {
		return this.dnsMode;
	}

	@Override
	public Configuration setDnsPreferenceMode(DNSPreference dnsPreference) {
		this.dnsMode = nullIfDefault(dnsPreference, Defaults.DEFAULT_DNS_PREFERENCE);
		return this;
	}

	@Override
	public Boolean isSavePasswords() {
		return this.savePasswords;
	}

	@Override
	public Preferences setSavePasswords(Boolean savePasswords) {
		this.savePasswords = nullIfDefault(savePasswords, Defaults.DEFAULT_SAVE_PASSWORDS);
		return this;
	}

	private <T> boolean loadPreferences(ConfigurationFile<T> cfg, T section) {
		Map<String, String> preferences = cfg.getSection(section);
		if ((preferences == null) || preferences.isEmpty()) { return false; }

		boolean changed = false;
		String v = null;

		v = preferences.get(Configuration.PREF_EXECUTABLE);
		if (v != null) {
			this.executable = v;
			changed = true;
		}

		v = preferences.get(Configuration.PREF_TIMEOUT);
		if (v != null) {
			try {
				int timeout = Integer.valueOf(v);
				if (timeout != Defaults.DEFAULT_TIMEOUT) {
					this.timeout = timeout;
					changed = true;
				}
			} catch (NumberFormatException e) {
				this.log.warn("Invalid timeout value [{}] in the configuration", v);
			}
		}

		v = preferences.get(Configuration.PREF_CIPHER);
		if ((v != null) && !Objects.equals(v, Defaults.DEFAULT_CIPHER)) {
			this.cipher = v;
		}

		v = preferences.get(Configuration.PREF_NO_ROUTE);
		if (v != null) {
			boolean b = Boolean.valueOf(v);
			if (b != Defaults.DEFAULT_NO_ROUTE) {
				this.noRoute = b;
				changed = true;
			}
		}

		v = preferences.get(Configuration.PREF_MTU);
		if (v != null) {
			try {
				int mtu = Integer.valueOf(v);
				if (mtu != Defaults.DEFAULT_MTU) {
					this.mtu = mtu;
					changed = true;
				}
			} catch (NumberFormatException e) {
				this.log.warn("Invalid MTU value [{}] in the configuration", v);
			}
		}

		v = preferences.get(Configuration.PREF_PPP_SYNCHRONICITY);
		if (v != null) {
			PPPSynchronicity mode = null;
			try {
				mode = PPPSynchronicity.valueOf(v);
			} catch (IllegalArgumentException e) {
				mode = Boolean.valueOf(v) ? PPPSynchronicity.SYNCHRONOUS : PPPSynchronicity.ASYNCHRONOUS;
				if (mode != Defaults.DEFAULT_PPP_SYNCHRONICITY) {
					this.pppAsyncMode = mode;
					changed = true;
				}
			}
		}

		v = preferences.get(Configuration.PREF_AUTO_RECONNECT);
		if (v != null) {
			AutoReconnectMode mode = null;
			try {
				mode = AutoReconnectMode.valueOf(v);
			} catch (IllegalArgumentException e) {
				mode = Boolean.valueOf(v) ? AutoReconnectMode.ENABLE : AutoReconnectMode.DISABLE;
				if (mode != Defaults.DEFAULT_AUTO_RECONNECT) {
					this.autoReconnect = mode;
					changed = true;
				}
			}
		}

		v = preferences.get(Configuration.PREF_DNS_PREFERENCE);
		if (v != null) {
			DNSPreference mode = null;
			try {
				mode = DNSPreference.valueOf(v);
			} catch (IllegalArgumentException e) {
				mode = DNSPreference.parseConfig(v);
				if ((mode != null) && (mode != Defaults.DEFAULT_DNS_PREFERENCE)) {
					this.dnsMode = mode;
					changed = true;
				}
			}
		}

		v = preferences.get(Configuration.PREF_SAVE_PASSWORDS);
		if (v != null) {
			boolean b = Boolean.valueOf(v);
			if (b != Defaults.DEFAULT_SAVE_PASSWORDS) {
				this.savePasswords = b;
				changed = true;
			}
		}
		return changed;
	}

	private ConnectionProfile deserializeConnectionProfile(String v) {
		// Format is server:port|user|password|domain
		String[] values = v.split("\\|");
		if (values.length != 4) {
			this.log.warn("Failed to import connection profile [{}] which was parsed as {}", v,
				Arrays.toString(values));
			return null;
		}

		if (!Configuration.isValidServerUrl(values[0])) {
			this.log.warn("Invalid server URL [{}] in connection profile [{}]", values[0], v);
			return null;
		}

		String s = values[0];
		String[] h = s.split(":");

		int port = Defaults.DEFAULT_PORT;
		if ((h.length > 1) && (h[1].length() > 0)) {
			port = Integer.valueOf(h[1]);
		}
		String user = values[1];
		CharBuffer buf = Configuration.decodePassword(values[2]);
		String pass = (buf != null ? buf.toString() : StringUtils.EMPTY);
		String domain = values[3];
		return new ConnectionProfile(h[0], port, domain, user, CharBuffer.wrap(pass));
	}

	private <T> boolean loadProfiles(ConfigurationFile<T> cfg, T section) {
		Map<String, String> cfgProfiles = cfg.getSection(section);
		if ((cfgProfiles == null) || cfgProfiles.isEmpty()) { return false; }

		this.profiles.clear();
		Map<String, ConnectionProfile> newProfiles = new LinkedHashMap<>();
		cfgProfiles.forEach((k, v) -> {
			try {
				ConnectionProfile p = deserializeConnectionProfile(v);
				if (p == null) { return; }
				String u = p.getUrl();
				newProfiles.put(u, p);
			} catch (Exception e) {
				this.log.warn("Failed to import a connection profile (value is [{}])", v, e);
				return;
			}
		});

		return this.profiles.addAll(newProfiles.values());
	}

	private <T> boolean loadCertificates(ConfigurationFile<T> cfg, T section) {
		return false;
	}

	public <T> boolean load(ConfigurationFile<T> cfg, T profilesSection, T preferencesSection, T certificatesSection) {
		boolean profiles = loadProfiles(cfg, profilesSection);
		boolean preferences = loadPreferences(cfg, preferencesSection);
		boolean certificates = loadCertificates(cfg, certificatesSection);
		return profiles || preferences || certificates;
	}

	private String serializeConnectionProfile(ConnectionProfile p) {
		// Format is server:port|user|password|domain
		return String.format("%s|%s|%s|%s", p.getUrl(), p.getUsername(), Configuration.encodePassword(p.getPassword()),
			p.getDomain());
	}

	private <T> void storeProfiles(ConfigurationFile<T> cfg, T section) {
		Map<String, String> newProfiles = cfg.getOrCreateSection(section);
		final AtomicInteger integer = new AtomicInteger(0);
		final int w = String.valueOf(this.profiles.size()).length() - 1;
		final String profileTagFormat;
		if (w <= 1) {
			profileTagFormat = "profile-%d";
		} else {
			// Add leading zeroes for ease of reading
			profileTagFormat = String.format("profile-%%0%dd", w);
		}

		this.profiles.forEach((p) -> newProfiles.put(String.format(profileTagFormat, integer.getAndIncrement()),
			serializeConnectionProfile(p)));
	}

	private <T> void storePreferences(ConfigurationFile<T> cfg, T section) {
		Map<String, String> preferences = cfg.getOrCreateSection(section);

		Stream.of( //
			Pair.of(Configuration.PREF_EXECUTABLE, this.executable), //
			Pair.of(Configuration.PREF_TIMEOUT, this.timeout), //
			Pair.of(Configuration.PREF_CIPHER, this.cipher), //
			Pair.of(Configuration.PREF_NO_ROUTE, this.noRoute), //
			Pair.of(Configuration.PREF_MTU, this.mtu), //
			Pair.of(Configuration.PREF_PPP_SYNCHRONICITY, this.pppAsyncMode), //
			Pair.of(Configuration.PREF_AUTO_RECONNECT, this.autoReconnect), //
			Pair.of(Configuration.PREF_DNS_PREFERENCE, this.dnsMode), //
			Pair.of(Configuration.PREF_SAVE_PASSWORDS, this.savePasswords) //
		)//
			.filter((p) -> p.getValue() != null) //
			.forEach((p) -> {
				Object v = p.getValue();
				if (v.getClass().isEnum()) {
					// Get the enum's name() value
					v = Enum.class.cast(v).name();
				}
				preferences.put(p.getKey(), v.toString());
			});
	}

	private <T> void storeCertificates(ConfigurationFile<T> cfg, T section) {
		// TODO: Implement this
	}

	public <T> void store(ConfigurationFile<T> cfg, T profilesSection, T preferencesSection, T certificatesSection) {
		storeProfiles(cfg, profilesSection);
		storePreferences(cfg, preferencesSection);
		storeCertificates(cfg, certificatesSection);
	}
}
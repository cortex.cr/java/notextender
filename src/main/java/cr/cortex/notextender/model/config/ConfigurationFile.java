package cr.cortex.notextender.model.config;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ConfigurationFile<SECTION> {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationFile.class);

	private static final Pattern SECTION = Pattern.compile("^\\s*\\[([^\\]]+)\\]\\s*$");
	private static final Pattern COMMENT = Pattern.compile("^\\s*[#;].*$");
	private static final Pattern SETTING = Pattern.compile("^\\s*([^=]+)[=:]\\s*(.*?)\\s*$");

	protected static Path buildUserHomePath(String fileName) {
		if (StringUtils.isEmpty(fileName)) { throw new IllegalArgumentException("Must provide a filename"); }
		Path home = Paths.get(System.getProperty("user.home"));

		ConfigurationFile.LOG.debug("Home path = [{}]", home);
		try {
			home = home.toRealPath();
			ConfigurationFile.LOG.debug("Canonical home path = [{}]", home);
		} catch (IOException e) {
			// Ignore...
			ConfigurationFile.LOG.warn("Failed to canonicalize the path [{}] - will continue with absolute path only",
				home, e);
			home = home.normalize();
		} finally {
			home = home.toAbsolutePath();
		}
		if (!Files.exists(home)) {
			ConfigurationFile.LOG.debug("The home directory [{}] does not exist", home);
			return null;
		}
		if (!Files.isDirectory(home)) {
			ConfigurationFile.LOG.debug("The home directory path [{}] is not a directory", home);
			return null;
		}

		return home.resolve(fileName);
	}

	private final Map<SECTION, Map<String, String>> sections = new LinkedHashMap<>();

	protected final Path defaultPath;

	protected ConfigurationFile() {
		this(null);
	}

	public final Path getDefaultPath() {
		return this.defaultPath;
	}

	protected ConfigurationFile(Path defaultPath) {
		this.defaultPath = defaultPath;
	}

	protected boolean isComment(String line) {
		return ConfigurationFile.COMMENT.matcher(line).matches();
	}

	protected abstract SECTION parseSection(String name);

	protected abstract String renderSection(SECTION section);

	protected BiFunction<Integer, String, Pair<String, String>> getEntryParser(SECTION sectionName) {
		return this::parseEntry;
	}

	protected Pair<String, String> parseEntry(int entries, String line) {
		Matcher m = ConfigurationFile.SETTING.matcher(line);
		if (!m.matches()) { return null; }
		return Pair.of(m.group(1), m.group(2));
	}

	protected BiFunction<Integer, Pair<String, String>, String> getEntryRenderer(SECTION sectionName) {
		return this::renderEntry;
	}

	protected String renderEntry(Integer index, Pair<String, String> entry) {
		return String.format("%s=%s", entry.getKey(), entry.getValue());
	}

	public final boolean loadFromDefault() throws IOException {
		if (this.defaultPath == null) { return false; }
		return load(this.defaultPath);
	}

	public final boolean load(Path path) throws IOException {
		return load(path, null);
	}

	public final boolean load(Path path, Charset charset) throws IOException {
		Objects.requireNonNull(path, "Must provide a path to load from");
		if (!Files.exists(path)) { return false; }
		if (!Files.isRegularFile(path)) { return false; }
		if (!Files.isReadable(path)) { return false; }
		load(Files.newInputStream(path), charset);
		return true;
	}

	public final void load(InputStream in) throws IOException {
		load(in, null);
	}

	public final void load(InputStream in, Charset charset) throws IOException {
		if (charset == null) {
			charset = Charset.defaultCharset();
		}
		load(new InputStreamReader(Objects.requireNonNull(in, "Must provide a non-null InputStream"), charset));
	}

	public final void load(Reader r) throws IOException {
		try (LineNumberReader in = new LineNumberReader(Objects.requireNonNull(r, "Must provide a non-null Reader"))) {
			String sectionLabel = null;
			SECTION section = null;
			BiFunction<Integer, String, Pair<String, String>> entryParser = null;
			Map<String, String> entries = null;
			while (true) {
				String line = in.readLine();
				if (line == null) {
					// Exit the loop if we're done reading
					break;
				}
				if (StringUtils.isBlank(line)) {
					// Ignore blank lines!
					continue;
				}

				if (isComment(line)) {
					ConfigurationFile.LOG.debug("Ignoring comment on line {}: [{}]", in.getLineNumber(), line);
					continue;
				}

				Matcher m = ConfigurationFile.SECTION.matcher(line);
				if (m.matches()) {
					// New section!!!
					if (section != null) {
						this.sections.put(section, entries);
					}

					sectionLabel = m.group(1);
					section = parseSection(sectionLabel);
					if (section == null) {
						ConfigurationFile.LOG.warn("Invalid section label [{}] on line {} ({})", sectionLabel,
							in.getLineNumber(), line);
						entryParser = null;
						entries = null;
						continue;
					}

					entryParser = getEntryParser(section);
					if (entryParser == null) {
						entries = null;
						ConfigurationFile.LOG.warn(
							"No viable parser found for section [{}] on line {} - the section will be left empty",
							sectionLabel, in.getLineNumber());
						continue;
					}
					entries = new LinkedHashMap<>();
				} else {
					if (section == null) {
						// We have an out-of-section line here...skip it
						ConfigurationFile.LOG.warn("Ignoring out-of-place line {}: [{}]", in.getLineNumber(), line);
						continue;
					}

					if (entryParser == null) {
						continue;
					}

					Pair<String, String> pair = entryParser.apply(entries.size(), line);
					if (pair == null) {
						ConfigurationFile.LOG.warn("Ignoring malformed line {}: [{}]", in.getLineNumber(), line);
						continue;
					}

					ConfigurationFile.LOG.debug("Adding setting [{}] with value [{}] to section [{}]", pair.getKey(),
						pair.getValue(), sectionLabel);
					entries.put(pair.getKey(), pair.getValue());
				}
			}

			if (section != null) {
				this.sections.put(section, entries);
			}
		}
	}

	public final void storeToDefault() throws IOException {
		if (this.defaultPath == null) { throw new IllegalStateException("No default location set for this instance"); }
		store(this.defaultPath);
	}

	public final void store(Path path) throws IOException {
		store(path, null);
	}

	public final void store(Path path, Charset charset) throws IOException {
		store(Files.newOutputStream(Objects.requireNonNull(path, "Must provide a path to store into")), charset);
	}

	public final void store(OutputStream out) throws IOException {
		store(out, null);
	}

	public final void store(OutputStream out, Charset charset) throws IOException {
		if (charset == null) {
			charset = Charset.defaultCharset();
		}
		store(new OutputStreamWriter(Objects.requireNonNull(out, "Must provide a non-null OutputStream to store into"),
			charset));
	}

	public final void store(Writer w) throws IOException {
		try (Writer out = new BufferedWriter(w)) {
			for (SECTION section : this.sections.keySet()) {
				BiFunction<Integer, Pair<String, String>, String> renderer = getEntryRenderer(section);
				if (renderer == null) {
					ConfigurationFile.LOG.warn("No renderer found for section [{}]", section);
					continue;
				}

				out.write(String.format("[%s]%n", renderSection(section)));
				Map<String, String> values = this.sections.get(section);
				int pos = 0;
				for (String key : values.keySet()) {
					if (pos > 0) {
						out.write(System.lineSeparator());
					}
					Pair<String, String> entry = Pair.of(key, values.getOrDefault(key, StringUtils.EMPTY));
					out.write(renderer.apply(pos, entry));
					pos++;
				}
				if (pos > 0) {
					out.write(System.lineSeparator());
				}
				out.write(System.lineSeparator());
			}
		}
	}

	public Collection<SECTION> getSectionNames() {
		return this.sections.keySet();
	}

	public boolean containsSection(SECTION name) {
		return this.sections.containsKey(name);
	}

	public Map<String, String> removeSection(SECTION name) {
		return this.sections.remove(name);
	}

	public Map<String, String> getSection(SECTION name) {
		return this.sections.get(name);
	}

	public Map<String, String> getOrCreateSection(SECTION name) {
		return this.sections.computeIfAbsent(name, (s) -> new LinkedHashMap<>());
	}
}
package cr.cortex.notextender.model.config;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ConnectionProfile implements Comparable<ConnectionProfile> {

	private static final String CIPHER = "AES/ECB/PKCS5Padding";
	private static final Charset CHARSET = StandardCharsets.UTF_8;
	private static final CharBuffer EMPTY_PASS = CharBuffer.allocate(0).asReadOnlyBuffer();
	private static final SecretKey KEY;
	static {
		byte[] key = new byte[16];
		new Random(System.nanoTime()).nextBytes(key);
		KEY = new SecretKeySpec(key, "AES");
	}

	protected static Cipher getCipher(boolean encrypt)
		throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		Cipher ret = Cipher.getInstance(ConnectionProfile.CIPHER);
		ret.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, ConnectionProfile.KEY);
		return ret;
	}

	private static final ByteBuffer encrypt(CharBuffer s) {
		if (s == null) {
			s = ConnectionProfile.EMPTY_PASS;
		}
		try {
			Cipher cipher = ConnectionProfile.getCipher(true);
			ByteBuffer in = ConnectionProfile.CHARSET.encode(s);
			ByteBuffer out = ByteBuffer.allocate(cipher.getOutputSize(in.remaining()));
			out.limit(cipher.doFinal(in, out));
			out.flip();
			return out.asReadOnlyBuffer();
		} catch (Exception e) {
			throw new RuntimeException("Failed to encrypt the given value", e);
		}
	}

	private static final CharBuffer decrypt(ByteBuffer in) {
		if (in == null) { return ConnectionProfile.EMPTY_PASS; }
		try {
			Cipher cipher = ConnectionProfile.getCipher(false);
			ByteBuffer out = ByteBuffer.allocate(cipher.getOutputSize(in.remaining()));
			out.limit(cipher.doFinal(in, out));
			out.flip();
			return ConnectionProfile.CHARSET.decode(out);
		} catch (Exception e) {
			throw new RuntimeException("Failed to decrypt the given value", e);
		}
	}

	private static final CharBuffer wrap(char[] s) {
		if ((s == null) || (s.length == 0)) { return ConnectionProfile.EMPTY_PASS; }
		return CharBuffer.wrap(s);
	}

	private final String server;
	private final int port;
	private final String domain;
	private final String username;
	private final ByteBuffer password;
	private final String url;

	public ConnectionProfile(String server, int port, String domain, String username, char[] password) {
		this(server, port, domain, username, ConnectionProfile.wrap(password));
	}

	public ConnectionProfile(String server, int port, String domain, String username, CharBuffer password) {
		this.server = server;
		this.port = port;
		this.domain = domain;
		this.username = username;
		this.password = ConnectionProfile.encrypt(password);

		String v = this.server;
		if (this.port != Defaults.DEFAULT_PORT) {
			v += ":" + this.port;
		}
		this.url = v;
	}

	public String getServer() {
		return this.server;
	}

	public int getPort() {
		return this.port;
	}

	public String getDomain() {
		return this.domain;
	}

	public String getUsername() {
		return this.username;
	}

	public CharBuffer getPassword() {
		if (this.password == null) { return ConnectionProfile.EMPTY_PASS; }
		return ConnectionProfile.decrypt(this.password.duplicate());
	}

	public String getUrl() {
		return this.url;
	}

	@Override
	public int compareTo(ConnectionProfile o) {
		if (o == null) { return 1; }
		int r = 0;
		r = String.CASE_INSENSITIVE_ORDER.compare(this.server, o.server);
		if (r != 0) { return r; }
		r = (this.port - o.port);
		if (r != 0) { return (r < 0 ? -1 : 1); }
		r = String.CASE_INSENSITIVE_ORDER.compare(this.domain, o.domain);
		if (r != 0) { return r; }
		r = String.CASE_INSENSITIVE_ORDER.compare(this.username, o.username);
		if (r != 0) { return r; }
		return 0;
	}

	@Override
	public String toString() {
		return getUrl();
	}
}
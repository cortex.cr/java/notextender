package cr.cortex.notextender.model.config;

import java.util.Objects;
import java.util.function.Supplier;

public interface Defaults {

	public static enum AutoReconnectMode {
		//
		DEFAULT(null), //
		ENABLE("auto"), //
		DISABLE("no"), //
		//
		;

		public final String param;

		private AutoReconnectMode(String param) {
			this.param = param;
		}
	}

	public static enum DNSPreference {
		//
		AUTO(null, null), //
		PREFER_REMOTE("prefer-remote", "preferRemote"), //
		ONLY_REMOTE("only-remote", "onlyRemote"), //
		ONLY_LOCAL("only-local", "onlyLocal"), //
		//
		;

		public final String param;
		public final String config;

		private DNSPreference(String param, String config) {
			this.param = param;
			this.config = config;
		}

		public static DNSPreference parseConfig(String config) {
			if (config == null) { return null; }
			for (DNSPreference m : DNSPreference.values()) {
				if (Objects.equals(config, m.config)) { return m; }
			}
			return null;
		}
	}

	public static enum PPPSynchronicity {
		//
		AUTO(null), //
		SYNCHRONOUS("sync"), //
		ASYNCHRONOUS("async"), //
		//
		;

		public final String param;

		private PPPSynchronicity(String param) {
			this.param = param;
		}
	}

	public static final String DEFAULT_EXE = "/usr/sbin/netExtender";
	public static final int DEFAULT_PORT = 443;
	public static final int DEFAULT_TIMEOUT = 30;
	public static final int DEFAULT_MTU = 1280;
	public static final String DEFAULT_CIPHER = "AUTO";
	public static final boolean DEFAULT_NO_ROUTE = false;
	public static final AutoReconnectMode DEFAULT_AUTO_RECONNECT = AutoReconnectMode.DEFAULT;
	public static final DNSPreference DEFAULT_DNS_PREFERENCE = DNSPreference.AUTO;
	public static final PPPSynchronicity DEFAULT_PPP_SYNCHRONICITY = PPPSynchronicity.AUTO;
	public static final boolean DEFAULT_SAVE_PASSWORDS = false;

	public default <T> boolean isDefault(T value, T def) {
		return (value != null) && Objects.equals(value, def);
	}

	public default <T> boolean isNotDefault(T value, T def) {
		return !isDefault(value, def);
	}

	public default <T> T getOrDefault(T value, T def) {
		return !isDefault(value, def) ? value : def;
	}

	public default <T> T nullIfDefault(T value, T def) {
		return !isDefault(value, def) ? value : null;
	}

	public default <T> T getOrDefault(Supplier<T> value, T def) {
		final T v = value.get();
		return !isDefault(v, def) ? v : null;
	}
}
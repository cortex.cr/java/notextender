package cr.cortex.notextender.model.config;

import java.nio.file.Path;
import java.util.function.BiFunction;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.model.config.MainConfigurationFile.Section;

public class MainConfigurationFile extends ConfigurationFile<Section> {

	public static enum Section {
		//
		PROFILES, //
		PREFERENCES, //
		TRUSTEDCERTS, //
		//
		;
	}

	private static final Logger LOG = LoggerFactory.getLogger(MainConfigurationFile.class);

	private static final Path DEFAULT_PATH;
	static {
		DEFAULT_PATH = ConfigurationFile.buildUserHomePath(".notextender");
		MainConfigurationFile.LOG.info("Default configuration path set to [{}]", MainConfigurationFile.DEFAULT_PATH);
	}

	public MainConfigurationFile() {
		super(MainConfigurationFile.DEFAULT_PATH);
	}

	@Override
	protected Section parseSection(String name) {
		try {
			return Section.valueOf(name.toUpperCase());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	protected String renderSection(Section section) {
		return section.name();
	}

	@Override
	protected BiFunction<Integer, Pair<String, String>, String> getEntryRenderer(Section sectionName) {
		switch (sectionName) {
			case PROFILES:
				return this::renderProfile;
			default:
				break;
		}
		return super.getEntryRenderer(sectionName);
	}

	protected String renderProfile(Integer pos, Pair<String, String> entry) {
		return String.format("profile-%d=%s", pos, entry.getValue());
	}
}
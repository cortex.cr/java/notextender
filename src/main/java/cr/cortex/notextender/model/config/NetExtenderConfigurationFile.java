package cr.cortex.notextender.model.config;

import java.nio.file.Path;
import java.util.Objects;
import java.util.function.BiFunction;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetExtenderConfigurationFile extends ConfigurationFile<String> {

	private static final Logger LOG = LoggerFactory.getLogger(NetExtenderConfigurationFile.class);

	public static final String SECTION_PROFILES = "profiles";
	public static final String SECTION_PREFERENCES = "preferences";
	public static final String SECTION_TRUSTEDCERTS = "trustedcerts";

	private static final Path DEFAULT_PATH;
	static {
		DEFAULT_PATH = ConfigurationFile.buildUserHomePath(".netextender");
		NetExtenderConfigurationFile.LOG.info("NetExtender configuration path set to [{}]",
			NetExtenderConfigurationFile.DEFAULT_PATH);
	}

	public NetExtenderConfigurationFile() {
		super(NetExtenderConfigurationFile.DEFAULT_PATH);
	}

	@Override
	protected String parseSection(String name) {
		return name;
	}

	@Override
	protected String renderSection(String section) {
		return section;
	}

	@Override
	protected BiFunction<Integer, String, Pair<String, String>> getEntryParser(String sectionName) {
		// Custom parser for profiles vs. everything else
		if (Objects.equals(NetExtenderConfigurationFile.SECTION_PROFILES, sectionName)) { return this::parseProfile; }
		return super.getEntryParser(sectionName);
	}

	protected Pair<String, String> parseProfile(Integer pos, String line) {
		return Pair.of(String.format("profile-%d", pos), line);
	}

	@Override
	protected BiFunction<Integer, Pair<String, String>, String> getEntryRenderer(String sectionName) {
		if (Objects.equals(NetExtenderConfigurationFile.SECTION_PROFILES, sectionName)) { return this::renderProfile; }
		return super.getEntryRenderer(sectionName);
	}

	protected String renderProfile(Integer pos, Pair<String, String> entry) {
		return entry.getValue();
	}
}
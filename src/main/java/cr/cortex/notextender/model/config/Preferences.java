package cr.cortex.notextender.model.config;

public interface Preferences extends Defaults {

	public String getExecutable();

	public Preferences setExecutable(String executable);

	public Integer getTimeout();

	public Preferences setTimeout(Integer timeout);

	public String getCipher();

	public Preferences setCipher(String cipher);

	public Boolean getNoRoute();

	public Preferences setNoRoute(Boolean noRoute);

	public Integer getMtu();

	public Preferences setMtu(Integer mtu);

	public PPPSynchronicity getPppSynchronicity();

	public Preferences setPppSynchronicity(PPPSynchronicity pppAsyncMode);

	public AutoReconnectMode getAutoReconnect();

	public Preferences setAutoReconnect(AutoReconnectMode autoReconnect);

	public DNSPreference getDnsPreference();

	public Preferences setDnsPreferenceMode(DNSPreference dnsMode);

	public Boolean isSavePasswords();

	public Preferences setSavePasswords(Boolean savePasswords);

	public default void copyFrom(Preferences p) {
		if (p == null) { return; }
		setExecutable(p.getExecutable()) //
			.setTimeout(p.getTimeout()) //
			.setCipher(p.getCipher()) //
			.setNoRoute(p.getNoRoute()) //
			.setMtu(p.getMtu()) //
			.setPppSynchronicity(p.getPppSynchronicity()) //
			.setAutoReconnect(p.getAutoReconnect()) //
			.setDnsPreferenceMode(p.getDnsPreference()) //
			.setSavePasswords(p.isSavePasswords()) //
		;
	}
}
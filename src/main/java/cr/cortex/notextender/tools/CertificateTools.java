package cr.cortex.notextender.tools;

import java.net.InetAddress;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

public class CertificateTools {

	private static final KeyStore NULL_KEYSTORE = null;

	private static final String DEFAULT_PROTOCOL = "SSL";

	public static class CapturingTrustManager implements X509TrustManager {
		private final X509TrustManager delegate;
		private List<X509Certificate> serverChain = null;

		public CapturingTrustManager(X509TrustManager delegate) {
			this.delegate = delegate;
		}

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			this.delegate.checkClientTrusted(chain, authType);
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			this.serverChain = Arrays.asList(chain);
			this.delegate.checkServerTrusted(chain, authType);
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return this.delegate.getAcceptedIssuers();
		}
	}

	public static Pair<Boolean, List<X509Certificate>> getCertificates(InetAddress address, int port) throws Exception {
		return CertificateTools.getCertificates(address, port, null);
	}

	public static Pair<Boolean, List<X509Certificate>> getCertificates(InetAddress address, int port, String protocol)
		throws Exception {
		Objects.requireNonNull(address, "Must provide an address to probe");
		if (port <= 0) { throw new IllegalArgumentException("Invalid port number"); }
		if (port > 0xFFFF) { throw new IllegalArgumentException("Invalid port number"); }
		if (StringUtils.isBlank(protocol)) {
			protocol = CertificateTools.DEFAULT_PROTOCOL;
		}

		TrustManagerFactory trustManagerFactory = TrustManagerFactory
			.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(CertificateTools.NULL_KEYSTORE);
		X509TrustManager defaultTrustManager = null;
		for (TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
			if (X509TrustManager.class.isInstance(trustManager)) {
				defaultTrustManager = X509TrustManager.class.cast(trustManager);
				break;
			}
		}

		CapturingTrustManager capture = new CapturingTrustManager(defaultTrustManager);
		SSLContext context = SSLContext.getInstance(protocol);
		context.init(null, new TrustManager[] {
			capture
		}, null);
		SSLSocketFactory socketFactory = context.getSocketFactory();
		boolean trusted = false;
		try (SSLSocket socket = SSLSocket.class.cast(socketFactory.createSocket(address, port))) {
			socket.startHandshake();
			trusted = true;
		} catch (SSLHandshakeException e) {
			// Not trusted... ignore this
		}

		return Pair.of(Boolean.valueOf(trusted), (capture.serverChain != null ? capture.serverChain : null));
	}

	/*-
	  String[] arrayOfString1 = getCertificateFields(this.mCertificate.getIssuerX500Principal().toString());
	  String[] arrayOfString2 = getCertificateFields(this.mCertificate.getSubjectX500Principal().toString());
	  this.txIssuedOn.setText(this.mCertificate.getNotBefore().toString());
	  this.txExpiredOn.setText(this.mCertificate.getNotAfter().toString());
	  this.txIssueToCN.setText(arrayOfString2[1] != null ? arrayOfString2[1] : "");
	  this.txIssueToO.setText(arrayOfString2[3] != null ? arrayOfString2[3] : "");
	  this.txIssueToOU.setText(arrayOfString2[2] != null ? arrayOfString2[2] : "");
	  this.txIssueToSN.setText(this.mCertificate.getSerialNumber().toString(16));
	  this.txIssueByCN.setText(arrayOfString1[1] != null ? arrayOfString1[1] : "");
	  this.txIssueByO.setText(arrayOfString1[3] != null ? arrayOfString1[3] : "");
	  this.txIssueByOU.setText(arrayOfString1[2] != null ? arrayOfString1[2] : "");
	  this.txSHA256Fingerprint.setText(getThumbPrint(this.mCertificate));
	  this.txSHA1Fingerprint.setText(getThumbPrint(this.mCertificate));
	
		Issued To:
			Common Name (CN):
			${cn}
			Organization (O):
			${o}
			Organizational Unit (OU):
			${ou}
			Serial Number:
			${serial}
		Issued By:
			Common Name (CN):
			${cn}
			Organization (O):
			${o}
			Organizational Unit (OU):
			${ou}
		Validity Period:
			Issued On:
			${issueDate}
			Expires On:
			${expirationDate}
			Fingerprints:
			${fingerprint}
	 */

	public static Pair<Boolean, List<X509Certificate>> getCertificates(String address, int port) throws Exception {
		return CertificateTools.getCertificates(address, port, null);
	}

	public static Pair<Boolean, List<X509Certificate>> getCertificates(String address, int port, String protocol)
		throws Exception {
		Objects.requireNonNull(address, "Must provide an address to probe");
		return CertificateTools.getCertificates(InetAddress.getByName(address), port, protocol);
	}
}
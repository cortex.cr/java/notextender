package cr.cortex.notextender.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.ProcessBuilder.Redirect;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.LongFunction;
import java.util.function.LongPredicate;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChildProcess {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@FunctionalInterface
	private interface TimeoutAction<T, E extends Throwable> {
		public T run() throws E;
	}

	public static abstract class PipedInput<STREAM extends Closeable, DATA> {

		private final Logger log = LoggerFactory.getLogger(getClass());
		private final BiPredicate<Long, DATA> consumer;

		protected PipedInput(BiPredicate<Long, DATA> consumer) {
			this.consumer = (consumer != null ? consumer : (r, d) -> Objects.nonNull(d));
		}

		/**
		 * <p>
		 * Open a readable version of the stream which will be able to produce {@code DATA}-typed
		 * data.
		 * </p>
		 *
		 * @param in
		 *            the {@link InputStream} to wrap around
		 *
		 * @return a readable version of the stream which will be able to produce {@code DATA}-typed
		 *         data.
		 *
		 * @throws IOException
		 */
		protected abstract STREAM openInput(InputStream in) throws IOException;

		/**
		 * <p>
		 * Read the next piece of {@code DATA} from the stream, returning {@code null} if there is
		 * no more data to be read and the reading loop should stop.
		 * </p>
		 *
		 * @param in
		 *            the {@code STREAM} previously obtained through {@link #openInput(InputStream)}
		 *
		 * @return data the {@code DATA} read from the stream, or {@code null} if there is no more
		 *         data to be read
		 *
		 * @throws IOException
		 */
		protected abstract DATA read(STREAM in) throws IOException;

		protected final long process(Supplier<InputStream> in) throws IOException {
			try (final STREAM s = openInput(in.get())) {
				long reads = 0;
				this.log.debug("Stream opened, starting the read loop");
				while (true) {
					DATA d = read(s);
					if (d == null) {
						this.log.debug("No data read, exiting the read loop");
						break;
					}

					reads++;
					this.log.debug("Read # {} = [{}]", reads, d);
					if (!this.consumer.test(reads, d)) {
						this.log.debug("Read # {} rejected, exiting the read loop", reads);
						break;
					}
				}
				this.log.debug("Read loop complete after {} reads", reads);
				return reads;
			}
		}
	}

	public static abstract class PipedOutput<STREAM extends Closeable, DATA> {

		private final Logger log = LoggerFactory.getLogger(getClass());
		private final LongFunction<DATA> producer;
		private final LongPredicate flushCheck;

		protected PipedOutput(LongFunction<DATA> producer, LongPredicate flushCheck) {
			this.producer = Objects.requireNonNull(producer, "Must supply a producer for the data to be written out");
			this.flushCheck = flushCheck;
		}

		/**
		 * <p>
		 * Open a writable version of the stream which will be able to consume {@code DATA}-typed
		 * data.
		 * </p>
		 *
		 * @param out
		 *            the {@link OutputStream} to wrap around
		 *
		 * @return a writable version of the stream which will be able to consume {@code DATA}-typed
		 *         data.
		 *
		 * @throws IOException
		 */
		protected abstract STREAM openOutput(OutputStream out) throws IOException;

		/**
		 * <p>
		 * Write out the given {@code DATA} to the stream. If the data is {@code null}, nothing is
		 * written.
		 * </p>
		 *
		 * @param out
		 *            the {@code STREAM} previously obtained through
		 *            {@link #openOutput(OutputStream)}
		 * @param data
		 *            the {@code DATA} to write to the stream
		 *
		 * @throws IOException
		 */
		protected abstract void write(STREAM out, DATA data) throws IOException;

		protected void flush(STREAM out) throws IOException {
			// Do nothing by default
		}

		protected final long process(Supplier<OutputStream> out) throws IOException {
			try (final STREAM s = openOutput(out.get())) {
				this.log.debug("Stream opened, starting the write loop");
				long writes = 0;
				while (true) {
					DATA d = this.producer.apply(writes + 1);
					if (d == null) {
						this.log.debug("No data produced for write # {}, exiting the write loop", writes + 1);
						break;
					}
					this.log.debug("Data for write # {} = [{}]", writes + 1, d);
					write(s, d);
					writes++;
					if ((this.flushCheck == null) || this.flushCheck.test(writes)) {
						flush(s);
					}
				}
				this.log.debug("Write loop complete after {} writes", writes);
				return writes;
			}
		}
	}

	private static final int MIN_BUFFER_SIZE = 512;
	private static final int DEF_BUFFER_SIZE = 4 * 1024;
	private static final int MAX_BUFFER_SIZE = 256 * 1024;

	private static <BUFFER extends Buffer> BUFFER allocateBuffer(int bufferSize, IntFunction<BUFFER> allocator) {
		if (bufferSize <= 0) {
			bufferSize = ChildProcess.DEF_BUFFER_SIZE;
		} else if (bufferSize < ChildProcess.MIN_BUFFER_SIZE) {
			bufferSize = ChildProcess.MIN_BUFFER_SIZE;
		} else if (bufferSize > ChildProcess.MAX_BUFFER_SIZE) {
			bufferSize = ChildProcess.MAX_BUFFER_SIZE;
		}
		return allocator.apply(bufferSize);
	}

	public static final class CharPipedInput extends PipedInput<Reader, CharSequence> {

		private final CharBuffer buffer;

		public CharPipedInput(BiPredicate<Long, CharSequence> consumer) {
			this(consumer, 0);
		}

		public CharPipedInput(BiPredicate<Long, CharSequence> consumer, int bufferSize) {
			super(consumer);
			this.buffer = ChildProcess.allocateBuffer(bufferSize, CharBuffer::allocate);
		}

		@Override
		protected Reader openInput(InputStream in) throws IOException {
			return new BufferedReader(new InputStreamReader(in));
		}

		@Override
		protected CharSequence read(Reader in) throws IOException {
			this.buffer.clear();
			in.read(this.buffer);
			this.buffer.flip();
			return this.buffer.toString();
		}
	}

	public static final class CharPipedOutput extends PipedOutput<Writer, CharSequence> {

		public CharPipedOutput(LongFunction<CharSequence> producer) {
			this(producer, null);
		}

		public CharPipedOutput(LongFunction<CharSequence> producer, LongPredicate flushCheck) {
			super(producer, flushCheck);
		}

		@Override
		protected Writer openOutput(OutputStream out) throws IOException {
			return new BufferedWriter(new OutputStreamWriter(out));
		}

		@Override
		protected void write(Writer out, CharSequence data) throws IOException {
			if (data != null) {
				out.write(data.toString());
			}
		}

		@Override
		protected void flush(Writer out) throws IOException {
			out.flush();
		}
	}

	public static final class LinePipedInput extends PipedInput<LineNumberReader, String> {

		public LinePipedInput(BiPredicate<Long, String> consumer) {
			super(consumer);
		}

		@Override
		protected LineNumberReader openInput(InputStream in) throws IOException {
			return new LineNumberReader(new BufferedReader(new InputStreamReader(in)));
		}

		@Override
		protected String read(LineNumberReader in) throws IOException {
			return in.readLine();
		}
	}

	public static final class LinePipedOutput extends PipedOutput<Writer, String> {

		public LinePipedOutput(LongFunction<String> producer) {
			this(producer, null);
		}

		public LinePipedOutput(LongFunction<String> producer, LongPredicate flushCheck) {
			super(producer, flushCheck);
		}

		@Override
		protected Writer openOutput(OutputStream out) throws IOException {
			return new BufferedWriter(new OutputStreamWriter(out));
		}

		@Override
		protected void write(Writer out, String data) throws IOException {
			if (data != null) {
				out.write(data);
				out.write(System.lineSeparator());
			}
		}

		@Override
		protected void flush(Writer out) throws IOException {
			out.flush();
		}
	}

	public static final class BinaryPipedInput extends PipedInput<ReadableByteChannel, byte[]> {

		private final ByteBuffer buffer;

		public BinaryPipedInput(BiPredicate<Long, byte[]> consumer) {
			this(consumer, 0);
		}

		public BinaryPipedInput(BiPredicate<Long, byte[]> consumer, int bufferSize) {
			super(consumer);
			this.buffer = ChildProcess.allocateBuffer(bufferSize, ByteBuffer::allocate);
		}

		@Override
		public ReadableByteChannel openInput(InputStream in) throws IOException {
			return Channels.newChannel(in);
		}

		@Override
		public byte[] read(ReadableByteChannel in) throws IOException {
			this.buffer.clear();
			if (in.read(this.buffer) < 0) { return null; }
			this.buffer.flip();
			byte[] data = new byte[this.buffer.limit() - 1];
			this.buffer.get(data);
			return data;
		}
	}

	public static final class BinaryPipedOutput extends PipedOutput<WritableByteChannel, byte[]> {

		private final ByteBuffer buffer;

		protected BinaryPipedOutput(LongFunction<byte[]> producer) {
			this(producer, 0, null);
		}

		protected BinaryPipedOutput(LongFunction<byte[]> producer, LongPredicate flushCheck) {
			this(producer, 0, flushCheck);
		}

		protected BinaryPipedOutput(LongFunction<byte[]> producer, int bufferSize) {
			this(producer, bufferSize, null);
		}

		protected BinaryPipedOutput(LongFunction<byte[]> producer, int bufferSize, LongPredicate flushCheck) {
			super(producer, flushCheck);
			this.buffer = ChildProcess.allocateBuffer(bufferSize, ByteBuffer::allocate);
		}

		@Override
		public WritableByteChannel openOutput(OutputStream out) throws IOException {
			return Channels.newChannel(out);
		}

		@Override
		public void write(WritableByteChannel out, byte[] data) throws IOException {
			if (data == null) { return; }
			this.buffer.clear();
			this.buffer.put(data).flip();
			out.write(this.buffer);
		}
	}

	public class Instance {

		private class OutputCapture<S extends Closeable, D> implements Callable<Long> {

			private final String cmd;
			private final String label;
			private final PipedInput<S, D> out;
			private final CountDownLatch latch;
			private final Supplier<InputStream> in;

			private OutputCapture(String cmd, String label, Supplier<InputStream> in, PipedInput<S, D> out,
				CountDownLatch latch) {
				this.cmd = cmd;
				this.label = label;
				this.out = out;
				this.in = in;
				this.latch = latch;
			}

			@Override
			public Long call() throws Exception {
				try {
					this.latch.await();
					return this.out.process(this.in);
				} catch (Throwable t) {
					ChildProcess.this.log.trace("Exception caught in the {} pipe for {}", this.label, this.cmd, t);
					throw t;
				} finally {
					ChildProcess.this.log.debug("{} pipe completed for {}", this.label, this.cmd);
				}
			}
		}

		private final Future<Long> inputFuture;
		private final ExecutorService pipes;
		private final Lock lock = new ReentrantLock();
		private final AtomicReference<Process> child = new AtomicReference<>();

		private Integer returnValue = null;

		private <R extends Closeable, W extends Closeable, D> Instance(BiConsumer<Integer, Throwable> end,
			PipedOutput<R, D> in, PipedInput<W, D> out, PipedInput<W, D> err) throws Exception {
			final String commandString = ChildProcess.this.builder.command().toString();
			int threads = 0;
			if (in != null) {
				threads++;
			}
			if (out != null) {
				threads++;
			}
			if (err != null) {
				threads++;
			}

			final CountDownLatch latch = new CountDownLatch(1);

			if (threads > 0) {
				// We add one more thread - the end hook
				threads++;
				this.pipes = Executors.newFixedThreadPool(threads);

				final List<Future<?>> futures = new ArrayList<>(threads - 1);

				if (in != null) {
					this.inputFuture = this.pipes.submit(() -> {
						try {
							latch.await();
							return in.process(() -> this.child.get().getOutputStream());
						} catch (Throwable t) {
							ChildProcess.this.log.trace("Exception caught in the STDIN pipe for {}", commandString, t);
							throw t;
						} finally {
							ChildProcess.this.log.debug("STDIN pipe completed for {}", commandString);
						}
					});
					futures.add(this.inputFuture);
				} else {
					this.inputFuture = CompletableFuture.completedFuture(0L);
				}

				if (out != null) {
					futures.add(this.pipes.submit(new OutputCapture<>(commandString, "STDOUT",
						() -> this.child.get().getInputStream(), out, latch)));
				}

				if (err != null) {
					futures.add(this.pipes.submit(new OutputCapture<>(commandString, "STDERR",
						() -> this.child.get().getErrorStream(), err, latch)));
				}

				this.pipes.submit(() -> {
					try {
						latch.await();
						if (end != null) {
							try {
								end.accept(this.child.get().waitFor(), null);
							} catch (InterruptedException e) {
								end.accept(null, e);
							}
						}
						return null;
					} catch (Throwable t) {
						ChildProcess.this.log.warn("Exception caught in the end hook for {}", commandString, t);
						throw t;
					} finally {
						futures.forEach((f) -> f.cancel(true));
						ChildProcess.this.log.debug("End hook completed for {}", commandString);
					}
				});

				// No more processes accepted
				this.pipes.shutdown();
			} else {
				this.pipes = null;
				this.inputFuture = CompletableFuture.completedFuture(0L);
			}

			this.lock.lock();
			try {
				try {
					ChildProcess.this.log.debug("Starting the child process {}", commandString);
					this.child.set(ChildProcess.this.builder.start());
					latch.countDown();
					ChildProcess.this.log.debug("Child process started: {}", commandString);
				} catch (IOException e) {
					ChildProcess.this.log.debug("Child process failed: {}", commandString);
					throw e;
				} catch (Exception e) {
					ChildProcess.this.log.debug("Error launching the process {}", commandString);
					e.printStackTrace();
					throw e;
				}
			} finally {
				this.lock.unlock();
			}
		}

		private <E extends Throwable> int joinOrDestroy(long amount, TimeUnit timeUnit, Consumer<Process> prepareAction,
			TimeoutAction<?, E> timeoutAction) throws InterruptedException, E {
			final long totalNanos = (timeUnit != null ? timeUnit.toNanos(amount) : -1);
			this.lock.lock();
			try {
				// First things first: wait until all the live threads are done
				if (this.returnValue != null) { return this.returnValue; }
				final Process child = this.child.get();
				if (child == null) { throw new IllegalStateException("This child process is no longer running"); }

				final long start = System.nanoTime();
				try {
					if (prepareAction != null) {
						prepareAction.accept(child);
					}
					if (timeUnit != null) {
						final long remainingNanos = totalNanos - (System.nanoTime() - start);
						if (child.waitFor(remainingNanos, TimeUnit.NANOSECONDS)) {
							this.returnValue = child.exitValue();
						} else {
							timeoutAction.run();
						}
					} else {
						this.returnValue = (child.isAlive() ? child.waitFor() : child.exitValue());
					}

					// This shouldn't take long
					if (this.pipes != null) {
						try {
							if (timeUnit != null) {
								final long remainingNanos = totalNanos - (System.nanoTime() - start);
								if (remainingNanos > 0) {
									this.pipes.awaitTermination(remainingNanos, TimeUnit.NANOSECONDS);
								}
							} else {
								this.pipes.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
							}
						} catch (InterruptedException e) {
							ChildProcess.this.log.warn("Interrupted while waiting for the child threads to exit");
						}
					}

					return this.returnValue;
				} finally {
					this.child.set(null);
					if (this.pipes != null) {
						this.pipes.shutdownNow();
						final long remainingNanos = totalNanos - (System.nanoTime() - start);
						if (remainingNanos > 0) {
							this.pipes.awaitTermination(remainingNanos, TimeUnit.NANOSECONDS);
						}
					}
				}
			} finally {
				this.lock.unlock();
			}
		}

		public int join() throws InterruptedException {
			try {
				return join(0, null);
			} catch (TimeoutException e) {
				throw new IllegalStateException("A time out is impossible when waiting forever");
			}
		}

		public int join(long amount, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
			return joinOrDestroy(amount, timeUnit, null, () -> {
				throw new TimeoutException("The process did not exit within the allotted timeout");
			});
		}

		public void destroy() throws InterruptedException {
			destroy(Long.MAX_VALUE, TimeUnit.DAYS);
		}

		public void destroy(long amount, TimeUnit timeUnit) throws InterruptedException {
			joinOrDestroy(amount, timeUnit, (p) -> {
				this.inputFuture.cancel(true);
				p.destroy();
			}, () -> this.child.get().destroyForcibly());
		}
	}

	private final ProcessBuilder builder;

	public ChildProcess(String... cmd) {
		this(null, cmd);
	}

	public ChildProcess(File workDir, String... cmd) {
		ProcessBuilder builder = new ProcessBuilder(cmd);
		if (workDir != null) {
			builder.directory(workDir);
		}
		builder.redirectError(Redirect.PIPE);
		builder.redirectOutput(Redirect.PIPE);
		this.builder = builder;
	}

	public <R extends Closeable, W extends Closeable, D> Instance start(PipedOutput<R, D> in, PipedInput<W, D> out)
		throws Exception {
		return start(null, in, out, null);
	}

	public <R extends Closeable, W extends Closeable, D> Instance start(PipedOutput<R, D> in, PipedInput<W, D> out,
		PipedInput<W, D> err) throws Exception {
		return start(null, in, out, err);
	}

	public <R extends Closeable, W extends Closeable, D> Instance start(BiConsumer<Integer, Throwable> end,
		PipedOutput<R, D> in, PipedInput<W, D> out, PipedInput<W, D> err) throws Exception {
		return new Instance(end, in, out, err);
	}
}
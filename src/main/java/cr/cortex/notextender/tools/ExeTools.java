package cr.cortex.notextender.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExeTools {

	private static final Logger LOG = LoggerFactory.getLogger(ExeTools.class);

	private static Set<Path> PATH_CANDIDATES;
	static {
		// Get the path variable
		Set<Path> candidates = null;
		final String path = System.getenv("PATH");
		if (!StringUtils.isEmpty(path)) {
			String[] entries = StringUtils.split(path, File.pathSeparatorChar);
			candidates = new LinkedHashSet<>();
			for (String s : entries) {
				if (StringUtils.isEmpty(s)) {
					continue;
				}
				candidates.add(Paths.get(s));
			}
		}

		if ((candidates != null) && !candidates.isEmpty()) {
			ExeTools.PATH_CANDIDATES = Collections.unmodifiableSet(candidates);
		} else {
			ExeTools.PATH_CANDIDATES = Collections.emptySet();
		}
	}

	private static Path canonicalize(Path p) {
		try {
			p = Objects.requireNonNull(p, "Must provide a path to canonicalize").toRealPath();
		} catch (IOException e) {
			if (ExeTools.LOG.isDebugEnabled()) {
				ExeTools.LOG.debug("Failed to canonicalize the path [{}]", p, e);
			} else {
				ExeTools.LOG.warn("Failed to canonicalize the path [{}]", p);
			}
		} finally {
			p = p.toAbsolutePath();
		}
		return p;
	}

	private static boolean isExecutable(Path exe) {
		if (!Files.exists(exe)) {
			ExeTools.LOG.debug("Path [{}] does not exist", exe);
			return false;
		}
		if (!Files.isRegularFile(exe)) {
			ExeTools.LOG.debug("Path [{}] is not a regular file", exe);
			return false;
		}

		if (!Files.isReadable(exe)) {
			ExeTools.LOG.debug("Path [{}] is not readable", exe);
			return false;
		}

		if (!Files.isExecutable(exe)) {
			ExeTools.LOG.debug("Path [{}] is not executable", exe);
			return false;
		}
		return true;
	}

	public static Path locateExecutable(String name) {
		return ExeTools.locateExecutable(name, ExeTools.PATH_CANDIDATES);
	}

	public static Path locateExecutable(String name, Set<Path> candidates) {
		Objects.requireNonNull(candidates, "Must supply a set of candidates to search in");
		if (candidates.isEmpty()) { return null; }

		for (Path p : candidates) {
			if (p == null) {
				continue;
			}
			p = ExeTools.canonicalize(p);
			Path exe = p.resolve(name);
			if (ExeTools.isExecutable(exe)) { return exe; }
		}
		return null;
	}
}
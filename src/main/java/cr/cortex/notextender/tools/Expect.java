package cr.cortex.notextender.tools;

import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.tools.ChildProcess.LinePipedInput;

public class Expect {

	private static final Logger LOG = LoggerFactory.getLogger(Expect.class);

	private static final String EXE_NAME = "expect";
	private static boolean SEARCH_DONE = false;
	private static Path EXPECT_CMD = null;

	public static synchronized Path getLocation() {
		if (!Expect.SEARCH_DONE) {
			Expect.EXPECT_CMD = Expect.locateCommand();
			Expect.SEARCH_DONE = true;
		}
		return Expect.EXPECT_CMD;
	}

	protected static Path locateCommand() {
		Path exe = ExeTools.locateExecutable(Expect.EXE_NAME);
		if (exe == null) {
			Expect.LOG.error("Could not find the 'expect' executable in the path");
			return null;
		}

		if (Expect.validateExpectExecutable(exe)) { return exe; }
		Expect.LOG.error("Could not validate the 'expect' executable at [" + exe + "]");
		return null;
	}

	protected static boolean validateExpectExecutable(Path exe) {
		try {
			// Run it with -v, check to see that the output is equals (CI) to "expect version 5."
			Expect.LOG.debug("Checking the executable at [{}]...", exe);
			ChildProcess child = new ChildProcess(exe.toString(), "-v");
			BlockingQueue<String> out = new LinkedBlockingDeque<>();
			LinePipedInput lineOut = new LinePipedInput((l, s) -> out.add(s));

			int rc = child.start(null, null, lineOut, lineOut).join();
			if (rc != 0) {
				Expect.LOG.debug("The executable at [{}] returned {}", exe, rc);
				return false;
			}
			// The first line must start with "expect version "
			String first = out.peek();
			if (StringUtils.isEmpty(first)) {
				Expect.LOG.debug("The executable at [{}] returned empty output", exe);
				return false;
			}

			if (!first.toLowerCase().startsWith("expect version 5")) {
				Expect.LOG.debug("The executable at [{}] returned the wrong output ({})", exe, first);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
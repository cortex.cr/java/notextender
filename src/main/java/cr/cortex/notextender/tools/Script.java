package cr.cortex.notextender.tools;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import cr.cortex.notextender.model.config.ConnectionProfile;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;

public class Script {

	private static final Configuration CFG;

	static {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);
		cfg.setClassForTemplateLoading(Script.class, "/templates");
		cfg.setDefaultEncoding(StandardCharsets.UTF_8.name());
		cfg.setLocale(Locale.US);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		CFG = cfg;
	}

	public static class Model {

		public static enum Datum {
			//
			USERNAME, //
			PASSWORD, //
			DOMAIN, //
			URL, //
			OTP, //
			//
			;

			private String generate() {
				return "{DATUM-" + name() + "-" + UUID.randomUUID() + "}";
			}
		}

		public static enum Query {
			//
			CERT, //
			//
			;

			private String generate(QueryOp op) {
				return "{" + op.name() + "-QUERY-" + name() + "-" + UUID.randomUUID() + "}";
			}
		}

		public static enum QueryOp {
			//
			BEGIN, //
			READ, //
			END, //
			//
			;
		}

		private final Component parentComponent;
		private final String script;

		private final Pattern parser;

		private final Map<String, Datum> dataMarkers;
		private final Map<Datum, Supplier<CharSequence>> dataValues;

		private final Map<String, Pair<Query, QueryOp>> queryMarkers;

		public Model(Component parentComponent, Template template, ConnectionProfile profile, String executable,
			String parameters) throws TemplateException {

			this.parentComponent = parentComponent;
			StringBuilder pattern = new StringBuilder();
			pattern.append("^(.*?)(?:\t(");
			boolean first;

			Map<String, Datum> dataMarkers = new LinkedHashMap<>();
			first = true;
			for (Datum d : Datum.values()) {
				String str = d.generate();
				dataMarkers.put(str, d);
				if (!first) {
					pattern.append("|");
				}
				pattern.append(Pattern.quote(str));
				first = false;
			}
			this.dataMarkers = Collections.unmodifiableMap(dataMarkers);

			Map<String, Pair<Query, QueryOp>> queryMarkers = new LinkedHashMap<>();
			for (Query q : Query.values()) {
				for (QueryOp o : QueryOp.values()) {
					String str = q.generate(o);
					queryMarkers.put(str, Pair.of(q, o));
					if (!first) {
						pattern.append("|");
					}
					pattern.append(Pattern.quote(str));
					first = false;
				}
			}
			this.queryMarkers = Collections.unmodifiableMap(queryMarkers);

			pattern.append("))(\\s*)$");
			this.parser = Pattern.compile(pattern.toString());

			Map<Datum, Supplier<CharSequence>> dataValues = new EnumMap<>(Datum.class);
			dataValues.put(Datum.DOMAIN, profile::getDomain);
			dataValues.put(Datum.USERNAME, profile::getUsername);
			dataValues.put(Datum.PASSWORD, profile::getPassword);
			dataValues.put(Datum.URL, profile::getUrl);
			dataValues.put(Datum.OTP, this::getOtp);
			this.dataValues = Collections.unmodifiableMap(dataValues);

			Map<String, Object> model = new HashMap<>();
			// Add al the datum markers
			for (String datumLabel : dataMarkers.keySet()) {
				Datum d = dataMarkers.get(datumLabel);
				model.put("read_" + d.name().toLowerCase(), datumLabel);
			}

			// Add all the query markers
			for (String queryLabel : queryMarkers.keySet()) {
				Pair<Query, QueryOp> p = queryMarkers.get(queryLabel);
				Query q = p.getKey();
				QueryOp o = p.getValue();
				model.put(o.name().toLowerCase() + "_" + q.name().toLowerCase(), queryLabel);
			}
			model.put("executable", executable);
			model.put("parameters", parameters);

			try (StringWriter w = new StringWriter()) {
				template.process(model, w);
				w.flush();
				this.script = w.toString();
			} catch (IOException e) {
				throw new UncheckedIOException("Unexpected IOException while working in memory?!?!", e);
			}
		}

		public Pair<String, String> getMarker(String rawString) {
			Matcher m = this.parser.matcher(rawString);
			if (!m.matches()) { return null; }
			return Pair.of(m.group(2), m.group(1));
		}

		public Pair<Datum, String> getDatumValue(String marker) {
			Datum datum = getDatum(marker);
			if (datum == null) { return null; }
			return Pair.of(datum, getDatumValue(datum));
		}

		public String getDatumValue(Datum datum) {
			if (datum == null) { return null; }
			Supplier<CharSequence> s = this.dataValues.get(datum);
			if (s == null) { return null; }
			CharSequence v = s.get();
			if (v == null) { return null; }
			return v.toString();
		}

		public Datum getDatum(String marker) {
			return this.dataMarkers.get(marker);
		}

		public Pair<Query, QueryOp> getQueryOp(String marker) {
			return this.queryMarkers.get(marker);
		}

		public int writeScript(String target) throws IOException {
			return writeScript(target, null);
		}

		public int writeScript(String target, Charset charset) throws IOException {
			return writeScript(Paths.get(target), charset);
		}

		public int writeScript(File target) throws IOException {
			return writeScript(target, null);
		}

		public int writeScript(File target, Charset charset) throws IOException {
			return writeScript(Objects.requireNonNull(target, "Must provide a File instance").toPath(), charset);
		}

		public int writeScript(Path target) throws IOException {
			return writeScript(target, null);
		}

		public int writeScript(Path target, Charset charset) throws IOException {
			if (charset == null) {
				charset = StandardCharsets.UTF_8;
			}
			try (WritableByteChannel out = Files.newByteChannel(target, StandardOpenOption.CREATE,
				StandardOpenOption.WRITE, StandardOpenOption.APPEND)) {
				return out.write(charset.encode(this.script));
			}
		}

		public String getOtp() {
			String result = JOptionPane.showInputDialog(this.parentComponent, "Please enter your OTP password",
				"MFA Verification", JOptionPane.QUESTION_MESSAGE);

			if (StringUtils.isBlank(result)) {
				// This should be the equivalent of a "cancel" operation ... but how?!?!
			}
			return result;
		}
	}

	private final Template template;

	public Script(String resourceName)
		throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {
		this.template = Script.CFG.getTemplate(resourceName);
	}

	public Model render(Component parentComponent, ConnectionProfile profile, String executable, String... parameters)
		throws TemplateException {
		return new Model(parentComponent, this.template, profile, executable, renderParameters(parameters));
	}

	private String renderParameters(String... parameters) {
		if (parameters.length < 1) { return StringUtils.EMPTY; }
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (String s : parameters) {
			if (StringUtils.isBlank(s)) {
				continue;
			}
			if (!first) {
				sb.append(' ');
			}
			// Make sure to quote the parameters for consumption by Expect
			sb.append('{').append(s).append('}');
			first = false;
		}
		return sb.toString();
	}
}
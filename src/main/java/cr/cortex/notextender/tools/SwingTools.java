package cr.cortex.notextender.tools;

import java.awt.Container;
import java.util.Objects;

import javax.swing.JComponent;

public class SwingTools {

	public static Container getAncestorOfClass(JComponent c) {
		return SwingTools.getAncestorOfClass(c, Container.class);
	}

	public static <C extends Container> C getAncestorOfClass(JComponent c, Class<C> klazz) {
		Objects.requireNonNull(c, "Must provide a component whose ancestor to seek");
		Objects.requireNonNull(klazz, "Must provide the class the ancestor must match");
		for (Container p = c.getParent(); p != null; p = p.getParent()) {
			if (klazz.isInstance(p)) { return klazz.cast(p); }
		}
		return null;
	}

}
package cr.cortex.notextender.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class AboutPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public AboutPanel() throws IOException {
		GridBagLayout gbl_aboutPanel = new GridBagLayout();
		setLayout(gbl_aboutPanel);

		JLabel lblNotExtenderLogo = new JLabel("");
		lblNotExtenderLogo.setIcon(new ImageIcon(getClass().getResource("/img/notextender128.png")));
		GridBagConstraints gbc_lblNotExtenderLogo = new GridBagConstraints();
		gbc_lblNotExtenderLogo.insets = new Insets(5, 5, 5, 5);
		gbc_lblNotExtenderLogo.gridx = 1;
		gbc_lblNotExtenderLogo.gridy = 1;
		add(lblNotExtenderLogo, gbc_lblNotExtenderLogo);

		JLabel lblNotExtender = new JLabel(String.format(
			"<html><center>NotExtender v%s<br/>© 2020 Cortex.cr, all rights reserved</center></html>", loadVersion()));
		GridBagConstraints gbc_lblNotExtender = new GridBagConstraints();
		gbc_lblNotExtender.insets = new Insets(5, 5, 5, 5);
		gbc_lblNotExtender.gridx = 1;
		gbc_lblNotExtender.gridy = 2;
		add(lblNotExtender, gbc_lblNotExtender);

		JLabel lblSonicWall = new JLabel(
			"<html><center>SonicWall, NetExtender, and related images,<br/>assets, and programs © 2019 SonicWall</center></html>");
		lblSonicWall.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblSonicWall = new GridBagConstraints();
		gbc_lblSonicWall.insets = new Insets(5, 5, 5, 5);
		gbc_lblSonicWall.gridx = 1;
		gbc_lblSonicWall.gridy = 4;
		add(lblSonicWall, gbc_lblSonicWall);

		JLabel lblAllOthers = new JLabel(
			"<html><center>All included open-source components are<br/>copyrighted to their respective holders</center></html>");
		GridBagConstraints gbc_lblAllOthers = new GridBagConstraints();
		gbc_lblAllOthers.gridx = 1;
		gbc_lblAllOthers.gridy = 6;
		add(lblAllOthers, gbc_lblAllOthers);
	}

	private String loadVersion() throws IOException {
		try (InputStream in = getClass().getResourceAsStream("/version.properties")) {
			Properties p = new Properties();
			p.load(in);
			return p.getProperty("version");
		}
	}

}

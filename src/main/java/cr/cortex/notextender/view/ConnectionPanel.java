package cr.cortex.notextender.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.PlainDocument;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.DBusAdapter;
import cr.cortex.notextender.DBusAdapter.InhibitorLock;
import cr.cortex.notextender.DBusAdapter.SignalHandler;
import cr.cortex.notextender.tools.ChildProcess;
import cr.cortex.notextender.tools.ChildProcess.LinePipedInput;
import cr.cortex.notextender.tools.ChildProcess.LinePipedOutput;
import cr.cortex.notextender.tools.Expect;
import cr.cortex.notextender.tools.Script;
import cr.cortex.notextender.tools.Script.Model.Datum;
import cr.cortex.notextender.tools.Script.Model.Query;
import cr.cortex.notextender.tools.Script.Model.QueryOp;
import cr.cortex.notextender.view.ConnectionProfilesPanel.Connection;

public class ConnectionPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final String USE_CTRL_C = "NetExtender connected successfully. Type \"Ctrl-c\" to disconnect...";
	private static final String EXPECT_SCRIPT = "script.exp";

	private class SafePlainDocument extends PlainDocument {
		private static final long serialVersionUID = 1L;

		public void append(String s) {
			writeLock();
			try {
				insertString(getLength(), s, null);
			} catch (BadLocationException e) {
				ConnectionPanel.this.log.error("Attempted to append text to a bad location: [{}]", s, e);
			} finally {
				writeUnlock();
			}
		}
	}

	private JTextArea logArea;
	private Consumer<String> appender;
	private JButton disconnectButton;
	private JCheckBox keepOpenCheck;
	private final Connection connection;
	private final ChildProcess process;
	private final AtomicReference<ChildProcess.Instance> instance = new AtomicReference<>();
	private final Thread shutdownHook;
	private final Runnable terminate;
	private AtomicReference<InhibitorLock> suspendInhibitor = new AtomicReference<>();

	private final File scriptFile;
	private Script.Model model = null;

	private final AtomicBoolean useCtrlC = new AtomicBoolean(false);
	private final BlockingQueue<String> interactiveInput = new LinkedBlockingQueue<>();

	/**
	 * Create the dialog.
	 */
	public ConnectionPanel(final Connection connection, Runnable terminate) throws IOException {
		super(new BorderLayout(0, 0));
		this.connection = connection;
		this.terminate = terminate;

		File scriptFile = File.createTempFile(".notExtender.", ".exp");
		try {
			scriptFile = scriptFile.getCanonicalFile();
		} catch (IOException e) {
			//
		} finally {
			this.scriptFile = scriptFile.getAbsoluteFile();
			this.scriptFile.deleteOnExit();
		}

		this.process = new ChildProcess(connection.getWorkDir(), Expect.getLocation().toString(),
			this.scriptFile.getAbsolutePath());
		setBorder(new EmptyBorder(5, 5, 5, 5));
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setAutoscrolls(true);
			add(scrollPane, BorderLayout.CENTER);
			{
				this.logArea = new JTextArea();
				final SafePlainDocument logDocument = new SafePlainDocument();
				this.logArea.setDocument(logDocument);
				this.appender = logDocument::append;
				scrollPane.setViewportView(this.logArea);
				this.logArea.setFont(new Font("Monospaced", Font.PLAIN, 14));
				this.logArea.setWrapStyleWord(true);
				this.logArea.setTabSize(4);
				this.logArea.setLineWrap(false);
				this.logArea.setEditable(false);
				DefaultCaret caret = new DefaultCaret();
				caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
				this.logArea.setCaret(caret);
			}
		}

		this.shutdownHook = new Thread() {
			@Override
			public void run() {
				ChildProcess.Instance i = ConnectionPanel.this.instance.get();
				if (i != null) {
					ConnectionPanel.this.log.warn("Forcing destruction due to JVM shutdown");
					try {
						i.destroy();
					} catch (InterruptedException e) {
						ConnectionPanel.this.log.warn("Interrupted waiting for the child process to exit");
					}
				}
			}
		};

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EmptyBorder(10, 10, 10, 10));
			add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new BorderLayout(5, 5));
			{
				{
					{
						JCheckBox wrapTextCheckBox = new JCheckBox("Wrap text");
						buttonPane.add(wrapTextCheckBox, BorderLayout.WEST);
						wrapTextCheckBox.setSelected(false);
						this.disconnectButton = new JButton("Disconnect");
						this.disconnectButton
							.setIcon(new ImageIcon(ConnectionPanel.class.getResource("/img/disconnect.png")));
						buttonPane.add(this.disconnectButton, BorderLayout.EAST);
						this.disconnectButton.setActionCommand("OK");

						this.keepOpenCheck = new JCheckBox("Keep open after disconnecting");
						this.keepOpenCheck.setToolTipText( //
							"<html>" + //
								"When checked, this dialog will remain open after disconnection.<br/>" + //
								"<br/>" + //
								"Otherwise, it will close automatically. It will also remain open any<br/>" + //
								"time the child process dies on its own, to preserve error output." + //
								"</html>" //
						);
						this.keepOpenCheck.setSelected(false);
						this.keepOpenCheck.setHorizontalAlignment(SwingConstants.CENTER);
						buttonPane.add(this.keepOpenCheck, BorderLayout.CENTER);

						this.disconnectButton.addActionListener((e) -> doDisconnect());
						wrapTextCheckBox
							.addActionListener((event) -> this.logArea.setLineWrap(wrapTextCheckBox.isSelected()));
					}
				}
			}
		}
	}

	private void doDisconnect() {
		final ChildProcess.Instance instance = this.instance.getAndSet(null);
		if (instance == null) {
			// It's morphed into the "Close" button...
			terminate();
			return;
		}

		// This should only happen once - the first time...
		this.keepOpenCheck.setEnabled(false);
		this.disconnectButton.setEnabled(false);
		this.disconnectButton.setText("Disconnecting...");
		new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				ConnectionPanel.this.log.info("Destroying the connection process...");
				if (ConnectionPanel.this.useCtrlC.get()) {
					// This is the "exit" command, which means wait for
					// the child to exit
					ConnectionPanel.this.interactiveInput.add("\3");
					instance.join();
				} else {
					// Not yet listening for the wait command, so we
					// forcibly kill the child process
					instance.destroy();
				}
				return null;
			}

			@Override
			protected void done() {
				Runtime.getRuntime().removeShutdownHook(ConnectionPanel.this.shutdownHook);
				appendLogOutput(String.format("%n%n<The child process has exited>"));
				ConnectionPanel.this.log.info("Connection process destroyed");
				if (!ConnectionPanel.this.keepOpenCheck.isSelected()) {
					terminate();
				}
			}
		}.execute();
	}

	private void appendLogOutput(String str) {
		appendLogOutput(str, null);
	}

	private void appendLogOutput(String str, Runnable after) {
		if (str != null) {
			SwingUtilities.invokeLater(() -> {
				this.appender.accept(str);
				if (after != null) {
					after.run();
				}
			});
		}
	}

	private boolean sendInput(String str) {
		if (str != null) {
			this.log.debug("Writing input: [{}]", str);
			ChildProcess.Instance instance = this.instance.get();
			if (instance != null) { return this.interactiveInput.add(str); }
		}
		return false;
	}

	private String getResponse(Query query, List<String> info) {
		// This is for easy display in a JOptionPane
		info.replaceAll((s) -> s + "<br/>");
		info.add(0, "<html>");
		info.add("</html>");

		// Now we present it... find a handler?
		StringBuilder msg = new StringBuilder();
		info.forEach(msg::append);
		int val = JOptionPane.showConfirmDialog(ConnectionPanel.this, msg.toString(), "Please confirm",
			JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		String response = "N";
		if (val == JOptionPane.YES_OPTION) {
			response = "Y";
		}
		return response;
	}

	protected synchronized void start() {
		if (ConnectionPanel.this.instance.get() != null) {
			throw new IllegalStateException("Can't run this window twice!");
		}
		// Start the process
		final AtomicReference<SignalHandler> disconnectHandler = new AtomicReference<>();
		try {
			final BlockingQueue<String> output = new LinkedBlockingQueue<>();
			final SwingWorker<Void, Void> instanceWorker = new SwingWorker<>() {
				@Override
				protected Void doInBackground() throws Exception {
					ConnectionPanel.this.log.info("Starting the connection process output listener");
					try {
						final Consumer<String> responder = ConnectionPanel.this::sendInput;
						final Consumer<String> defaultConsumer = ConnectionPanel.this::appendLogOutput;
						Consumer<String> consumer = defaultConsumer;
						final List<String> swallowed = new ArrayList<>();
						boolean ignoreBlanks = false;
						while (true) {
							String str = null;
							try {
								str = output.take();
							} catch (InterruptedException e) {
								break;
							}

							if (ignoreBlanks && StringUtils.isBlank(str)) {
								continue;
							}
							ignoreBlanks = false;

							ConnectionPanel.this.log.trace("Captured output: [{}]", str);

							Pair<String, String> marker = ConnectionPanel.this.model.getMarker(str);
							if (marker != null) {
								// Spit out the prompt immediately
								consumer.accept(marker.getValue());

								// We have data to input!! Let's do that!
								Pair<Datum, String> datumValue = ConnectionPanel.this.model
									.getDatumValue(marker.getKey());
								if (datumValue != null) {
									String value = datumValue.getValue();
									if (value != null) {
										// We know what to input!! Let's spit it out, complete with
										// a \r terminator. This needs to be synchronous - we cannot
										// continue until after this has happened.
										responder.accept(datumValue.getValue());
									} else {
										doDisconnect();
										break;
									}
								} else {
									Pair<Query, QueryOp> queryMarker = ConnectionPanel.this.model
										.getQueryOp(marker.getKey());
									if (queryMarker != null) {
										// We're being asked to handle a query!!
										switch (queryMarker.getValue()) {
											case BEGIN:
												// Start swallowing input
												swallowed.clear();
												consumer = swallowed::add;
												if (StringUtils.isBlank(marker.getValue())) {
													continue;
												}
												break;

											case READ:
												// The question has been asked, present it
												consumer.accept(marker.getValue());

												// First, remove any blank lines
												swallowed.removeIf(StringUtils::isBlank);
												// Next, remove any leading/trailing whitespace
												swallowed.replaceAll(StringUtils::strip);

												String sig = swallowed.stream() //
													.filter((s) -> s.startsWith("SHA1[")) //
													.findFirst() //
													.orElse("") //
												;

												// Ok...ask the user how to respond
												String response = getResponse(queryMarker.getKey(), swallowed);
												if (response == null) {
													doDisconnect();
													break;
												}

												responder.accept(response);

												// Find the SHA1 line
												if (StringUtils.isNotBlank(sig)) {
													defaultConsumer.accept(
														String.format("Trusted certificate signature:%n\t%s%n", sig));
												}
												break;
											case END:
												// Stop swallowing input
												// (until the next non-blank line)
												consumer.accept(marker.getValue());
												ignoreBlanks = true;
												consumer = defaultConsumer;
												continue;
										}
									}
								}
							} else {
								consumer.accept(str);
								if (StringUtils.equalsIgnoreCase(ConnectionPanel.USE_CTRL_C, StringUtils.strip(str))) {
									ConnectionPanel.this.useCtrlC.set(true);
								}
							}

						}
					} catch (Throwable t) {
						ConnectionPanel.this.log
							.debug("Caught exception while listening for the connection process's output", t);
					}
					return null;
				}

				@Override
				protected void done() {
					ConnectionPanel.this.log.info("Connection process output listener done");
					ConnectionPanel.this.disconnectButton.setText("Close");
					ConnectionPanel.this.disconnectButton.setEnabled(true);
				}
			};
			instanceWorker.execute();

			Script script = new Script(ConnectionPanel.EXPECT_SCRIPT);

			this.model = script.render(ConnectionPanel.this, this.connection.connectionProfile,
				this.connection.executable.getAbsolutePath(), this.connection.getPreferenceParameters());
			this.model.writeScript(this.scriptFile);

			final LinePipedInput reader = new LinePipedInput((l, s) -> output.add(s + System.lineSeparator()));
			final LinePipedOutput input = new LinePipedOutput((l) -> {
				try {
					return this.interactiveInput.take();
				} catch (InterruptedException e) {
					// Ignore and fail
					throw new RuntimeException("Failed to get the next line of interactive input", e);
				}
			});

			disconnectHandler.set(DBusAdapter.addSuspendHandler((b) -> {
				if (b) {
					doDisconnect();
				}
			}));
			this.suspendInhibitor.set(DBusAdapter.delaySuspendAndShutdown("VPN Connection cleanup"));
			Runtime.getRuntime().addShutdownHook(this.shutdownHook);
			ConnectionPanel.this.instance.set(ConnectionPanel.this.process.start((rc, t) -> {
				// Pipe all remaining captured output
				try (InhibitorLock suspendLock = this.suspendInhibitor.get()) {
					disconnectHandler.get().close();
				} finally {
					this.suspendInhibitor.set(null);
				}
				while (!output.isEmpty()) {
					Thread.yield();
				}
				instanceWorker.cancel(true);
				ConnectionPanel.this.instance.set(null);
				this.model = null;
				this.scriptFile.delete();
			}, input, reader, reader));
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			appendLogOutput(sw.toString());
			this.disconnectButton.setText("Close");
			this.disconnectButton.setEnabled(true);
		} finally {
			try (SignalHandler sh = disconnectHandler.get()) {
				Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
			}
		}
	}

	protected void terminate() {
		if (this.terminate != null) {
			this.terminate.run();
		}
	}

}

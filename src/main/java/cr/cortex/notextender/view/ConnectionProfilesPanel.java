package cr.cortex.notextender.view;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicComboBoxEditor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.model.config.Configuration;
import cr.cortex.notextender.model.config.ConnectionProfile;
import cr.cortex.notextender.model.config.Defaults;
import cr.cortex.notextender.model.config.Defaults.AutoReconnectMode;
import cr.cortex.notextender.model.config.Defaults.DNSPreference;
import cr.cortex.notextender.model.config.Defaults.PPPSynchronicity;
import cr.cortex.notextender.model.config.Preferences;
import cr.cortex.notextender.tools.SwingTools;

public class ConnectionProfilesPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public class Connection {
		public final File executable;

		public final ConnectionProfile connectionProfile;

		public final Integer timeout;
		public final String cipher;
		public final Boolean noRoute;
		public final Integer mtu;
		public final PPPSynchronicity pppAsyncMode;
		public final AutoReconnectMode autoReconnect;
		public final DNSPreference dnsPreferenceMode;

		private Connection(ConnectionProfile connectionProfile, Preferences preferences) {
			this.connectionProfile = connectionProfile;
			this.executable = new File(preferences.getExecutable());
			this.timeout = preferences.getTimeout();
			this.cipher = preferences.getCipher();
			this.noRoute = preferences.getNoRoute();
			this.mtu = preferences.getMtu();
			this.pppAsyncMode = preferences.getPppSynchronicity();
			this.autoReconnect = preferences.getAutoReconnect();
			this.dnsPreferenceMode = preferences.getDnsPreference();
		}

		public File getWorkDir() {
			return this.executable.getParentFile();
		}

		private <T> boolean isDefault(T value, T def) {
			return (value != null) && Objects.equals(value, def);
		}

		public String[] getPreferenceParameters() {
			List<String> cmd = new LinkedList<>();
			if (!isDefault(this.timeout, Defaults.DEFAULT_TIMEOUT)) {
				cmd.add("--timeout");
				cmd.add(String.valueOf(this.timeout));
			}

			if (!isDefault(this.cipher, Defaults.DEFAULT_CIPHER)) {
				cmd.add("--cipher=" + this.cipher);
			}

			if (!isDefault(this.noRoute, Defaults.DEFAULT_NO_ROUTE)) {
				cmd.add("--no-routes");
			}

			if (!isDefault(this.mtu, Defaults.DEFAULT_MTU)) {
				cmd.add("--mtu=" + this.mtu);
			}

			if (!isDefault(this.pppAsyncMode, Defaults.DEFAULT_PPP_SYNCHRONICITY)) {
				cmd.add("--ppp-" + this.pppAsyncMode.param);
			}

			if (!isDefault(this.autoReconnect, Defaults.DEFAULT_AUTO_RECONNECT)) {
				cmd.add("--" + this.autoReconnect.param + "-reconnect");
			}

			if (!isDefault(this.dnsPreferenceMode, Defaults.DEFAULT_DNS_PREFERENCE)) {
				cmd.add("--dns-" + this.dnsPreferenceMode.param);
			}
			return cmd.toArray(new String[0]);
		}
	}

	private final Map<String, ConnectionProfile> profiles = Collections.synchronizedMap(new TreeMap<>());
	private final DocumentListener connectButtonListener = new DocumentListener() {
		@Override
		public void insertUpdate(DocumentEvent e) {
			enableOrDisableConnectButton();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			enableOrDisableConnectButton();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			enableOrDisableConnectButton();
		}
	};

	private final Logger log = LoggerFactory.getLogger(getClass());

	private JComboBox<String> profileCombo;
	private JTextField domainField;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton connectButton;

	private final Preferences preferences;
	private final Runnable saveConfigurations;

	public ConnectionProfilesPanel(Image icon, Preferences preferences, Runnable saveConfigurations) {
		this.preferences = Objects.requireNonNull(preferences,
			"Must provide a Preferences instance this object will use");
		this.saveConfigurations = (saveConfigurations != null) ? saveConfigurations : UITools.NOOP;
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		setLayout(gbl_mainPanel);

		Insets insets = new Insets(5, 5, 5, 5);

		final JLabel lblServer = new JLabel("Server");
		GridBagConstraints gbc_lblServer = new GridBagConstraints();
		gbc_lblServer.anchor = GridBagConstraints.EAST;
		gbc_lblServer.insets = insets;
		gbc_lblServer.gridx = 1;
		gbc_lblServer.gridy = 1;
		add(lblServer, gbc_lblServer);

		this.profileCombo = new JComboBox<>();
		this.profileCombo.setEditable(true);
		lblServer.setLabelFor(this.profileCombo);
		GridBagConstraints gbc_profileCombo = new GridBagConstraints();
		gbc_profileCombo.fill = GridBagConstraints.HORIZONTAL;
		gbc_profileCombo.insets = insets;
		gbc_profileCombo.gridx = 2;
		gbc_profileCombo.gridy = 1;
		add(this.profileCombo, gbc_profileCombo);
		this.profileCombo.setToolTipText(
			"Specify the SSL VPN or UTM device either in fully-qualified domain name (FQDN) or IP address.\n"
				+ "\nSpecify an alternate port by adding a colon (i.e. server:port)");

		final Function<Object, String> updateOnSelection = (o) -> {
			String s = StringUtils.EMPTY;
			if (this.domainField == null) { return s; }
			if (this.usernameField == null) { return s; }
			if (this.passwordField == null) { return s; }

			if (o != null) {
				s = o.toString();
			}

			String domain = StringUtils.EMPTY;
			String username = StringUtils.EMPTY;
			String password = StringUtils.EMPTY;

			ConnectionProfile p = null;
			if ((o != null) && this.profiles.containsKey(o)) {
				p = this.profiles.get(o);
				domain = p.getDomain();
				username = p.getUsername();
				password = p.getPassword().toString();
			}
			ConnectionProfilesPanel.this.domainField.setText(domain);
			ConnectionProfilesPanel.this.usernameField.setText(username);
			ConnectionProfilesPanel.this.passwordField.setText(password);
			enableOrDisableConnectButton();
			return s;
		};

		this.profileCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() != ItemEvent.SELECTED) { return; }
				updateOnSelection.apply(e.getItem());
			}
		});

		this.profileCombo.setEditor(new BasicComboBoxEditor() {
			{
				this.editor.setInputVerifier(new InputVerifier() {
					@Override
					public boolean verify(JComponent input) {
						if (!JTextField.class.isInstance(input)) { return true; }
						JTextField field = JTextField.class.cast(input);
						String str = field.getText();
						final boolean verified = StringUtils.isBlank(str) || Configuration.isValidServerUrl(str);
						lblServer.setForeground(verified ? Color.BLACK : Color.RED);
						ConnectionProfilesPanel.this.profileCombo.setForeground(verified ? Color.WHITE : Color.RED);
						return verified;
					}
				});
			}

			@Override
			protected JTextField createEditorComponent() {
				return new JTextField();
			}

			@Override
			public void setItem(Object anObject) {
				this.editor.setText(updateOnSelection.apply(anObject));
			}

		});

		JLabel lblDomain = new JLabel("Domain");
		GridBagConstraints gbc_lblDomain = new GridBagConstraints();
		gbc_lblDomain.anchor = GridBagConstraints.EAST;
		gbc_lblDomain.insets = insets;
		gbc_lblDomain.gridx = 1;
		gbc_lblDomain.gridy = 2;
		add(lblDomain, gbc_lblDomain);

		this.domainField = new JTextField();
		new UndoWrapper<>(this.domainField);
		lblDomain.setLabelFor(this.domainField);
		GridBagConstraints gbc_domainField = new GridBagConstraints();
		gbc_domainField.anchor = GridBagConstraints.WEST;
		gbc_domainField.insets = insets;
		gbc_domainField.gridx = 2;
		gbc_domainField.gridy = 2;
		add(this.domainField, gbc_domainField);
		this.domainField.setColumns(20);
		this.domainField.getDocument().addDocumentListener(this.connectButtonListener);

		JLabel lblUsername = new JLabel("Username");
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = insets;
		gbc_lblUsername.gridx = 1;
		gbc_lblUsername.gridy = 3;
		add(lblUsername, gbc_lblUsername);

		this.usernameField = new JTextField();
		new UndoWrapper<>(this.usernameField);
		lblUsername.setLabelFor(this.usernameField);
		GridBagConstraints gbc_usernameField = new GridBagConstraints();
		gbc_usernameField.anchor = GridBagConstraints.WEST;
		gbc_usernameField.insets = insets;
		gbc_usernameField.gridx = 2;
		gbc_usernameField.gridy = 3;
		add(this.usernameField, gbc_usernameField);
		this.usernameField.setColumns(20);
		this.usernameField.getDocument().addDocumentListener(this.connectButtonListener);

		JLabel lblPassword = new JLabel("Password");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.insets = insets;
		gbc_lblPassword.gridx = 1;
		gbc_lblPassword.gridy = 4;
		add(lblPassword, gbc_lblPassword);

		this.passwordField = new JPasswordField();
		new UndoWrapper<>(this.passwordField);
		lblPassword.setLabelFor(this.passwordField);
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.anchor = GridBagConstraints.WEST;
		gbc_passwordField.insets = insets;
		gbc_passwordField.gridx = 2;
		gbc_passwordField.gridy = 4;
		add(this.passwordField, gbc_passwordField);
		this.passwordField.setColumns(20);
		this.passwordField.getDocument().addDocumentListener(this.connectButtonListener);

		this.connectButton = new JButton("Connect");
		this.connectButton.setEnabled(false);
		this.connectButton.addActionListener((event) -> {
			Object selectedItem = ConnectionProfilesPanel.this.profileCombo.getSelectedItem();
			String server = selectedItem.toString();

			int port = Defaults.DEFAULT_PORT;

			String[] split = server.split(":");
			server = split[0];
			if (split.length > 1) {
				// No need to protect here, this will have already been validated
				port = Integer.valueOf(split[1]);
			}

			String domain = this.domainField.getText();
			String username = this.usernameField.getText();

			ConnectionProfile p = new ConnectionProfile(server, port, domain, username,
				this.passwordField.getPassword());

			// Only add to the combo if it's a new profile
			if (!this.profiles.containsKey(p.getUrl())) {
				this.profileCombo.addItem(p.getUrl());
			}

			this.profiles.put(p.getUrl(), p);
			Connection c = new Connection(p, this.preferences);
			this.saveConfigurations.run();

			final JFrame frame = SwingTools.getAncestorOfClass(this, JFrame.class);

			// Prepare the dialog
			final JDialog dialog = new JDialog(frame);
			Rectangle bounds = new Rectangle(100, 100, 750, 500);
			if (frame != null) {
				dialog.setLocationRelativeTo(frame);
				bounds = frame.getBounds();
			}
			dialog.setBounds(bounds);
			dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			dialog.setIconImage(icon);
			dialog.setTitle("Connection Status");
			dialog.setModalityType(ModalityType.APPLICATION_MODAL);

			// Add the connection stuff
			final ConnectionPanel connectionPanel;
			try {
				connectionPanel = new ConnectionPanel(c, () -> {
					dialog.setVisible(false);
					dialog.dispose();
				});
			} catch (IOException e) {
				this.log.error("Failed to create a temporary file", e);
				JOptionPane.showMessageDialog(ConnectionProfilesPanel.this,
					"Failed to create a required temporary file, please check the logs", "Error",
					JOptionPane.ERROR_MESSAGE);
				return;
			}
			dialog.getContentPane().add(connectionPanel);
			dialog.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentMoved(ComponentEvent e) {
					// For now, disable the frame moving...
					// frame.setLocation(dialog.getLocation());
				}

				@Override
				public void componentShown(ComponentEvent event) {
					connectionPanel.start();
				}
			});

			// Fire it all off
			dialog.setVisible(true);
		});
		this.connectButton.setIcon(new ImageIcon(getClass().getResource("/img/connect.png")));
		GridBagConstraints gbc_connectButton = new GridBagConstraints();
		gbc_connectButton.insets = new Insets(0, 0, 5, 5);
		gbc_connectButton.anchor = GridBagConstraints.EAST;
		gbc_connectButton.gridx = 2;
		gbc_connectButton.gridy = 5;
		add(this.connectButton, gbc_connectButton);
	}

	boolean enableOrDisableConnectButton() {
		// First things first: do we have a valid executable?
		boolean valid = true;

		if (valid) {
			Object o = this.profileCombo.getSelectedItem();
			valid &= (o != null) && StringUtils.isNotBlank(o.toString());
		}

		if (valid) {
			valid &= StringUtils.isNotBlank(this.domainField.getText());
		}

		if (valid) {
			valid &= StringUtils.isNotBlank(this.usernameField.getText());
		}

		if (valid) {
			valid &= (this.passwordField.getPassword().length > 0);
		}

		this.connectButton.setEnabled(valid);
		return valid;
	}

	boolean loadProfiles(Configuration cfg) {
		final AtomicBoolean changed = new AtomicBoolean();
		cfg.profiles.forEach((p) -> {
			String u = p.getUrl();
			this.profiles.put(u, p);
			this.profileCombo.addItem(u);
			changed.set(true);
		});
		return changed.get();
	}

	boolean saveProfiles(Configuration cfg) {
		cfg.profiles.clear();

		Collection<ConnectionProfile> profiles = this.profiles.values();
		Boolean save = this.preferences.isSavePasswords();
		if (save == null) {
			save = Defaults.DEFAULT_SAVE_PASSWORDS;
		}
		if (!save) {
			// Sanitize the passwords before storage
			Collection<ConnectionProfile> sanitized = new ArrayList<>(profiles.size());
			for (ConnectionProfile p : profiles) {
				sanitized.add(
					new ConnectionProfile(p.getServer(), p.getPort(), p.getDomain(), p.getUsername(), (char[]) null));
			}
			profiles = sanitized;
		}

		return cfg.profiles.addAll(profiles);
	}
}
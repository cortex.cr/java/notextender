package cr.cortex.notextender.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.model.config.Configuration;
import cr.cortex.notextender.model.config.Defaults;
import cr.cortex.notextender.model.config.MainConfigurationFile;
import cr.cortex.notextender.model.config.NetExtenderConfigurationFile;
import cr.cortex.notextender.tools.Expect;

public class GUI implements Runnable {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Image mainIcon = Toolkit.getDefaultToolkit()
		.getImage(getClass().getResource("/img/notextender128.png"));
	private final JFrame mainFrame;
	private final Collection<Predicate<Configuration>> saveFunctions;
	private final Collection<Predicate<Configuration>> loadFunctions;

	/**
	 * @wbp.parser.entryPoint
	 */
	public GUI() throws IOException {
		this.mainFrame = new JFrame();
		this.mainFrame.setIconImage(this.mainIcon);
		this.mainFrame.setResizable(false);
		this.mainFrame.setTitle("NotExtender");
		this.mainFrame.setBounds(100, 100, 750, 500);
		this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.mainFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				saveConfigurations();
			}
		});

		JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		this.mainFrame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

		Collection<Predicate<Configuration>> saveFunctions = new ArrayList<>(4);
		Collection<Predicate<Configuration>> loadFunctions = new ArrayList<>(4);

		PreferencesPanel pPanel = new PreferencesPanel(this::saveConfigurations);
		saveFunctions.add(pPanel::savePreferences);
		loadFunctions.add(pPanel::loadPreferences);

		ConnectionProfilesPanel cPanel = new ConnectionProfilesPanel(this.mainIcon, pPanel, this::saveConfigurations);
		saveFunctions.add(cPanel::saveProfiles);
		loadFunctions.add(cPanel::loadProfiles);

		AboutPanel aPanel = new AboutPanel();

		// Add the tabs in the correct order
		tabbedPane.addTab("Connect", new ImageIcon(getClass().getResource("/img/notextender16.png")), cPanel, null);
		tabbedPane.addTab("Preferences", new ImageIcon(getClass().getResource("/img/settings.png")), pPanel, null);
		tabbedPane.addTab("About", new ImageIcon(getClass().getResource("/img/info.png")), aPanel, null);

		this.saveFunctions = Collections.unmodifiableCollection(saveFunctions);
		this.loadFunctions = Collections.unmodifiableCollection(loadFunctions);

		tabbedPane.setSelectedIndex(1);
		if (Expect.getLocation() == null) {
			JOptionPane.showMessageDialog(this.mainFrame,
				"Can't continue without the expect program, please install it first", "Error",
				JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

		// First things first: do we have our own configuations saved?
		if (!loadConfigurations() && importNetExtenderConfigurations()) {
			// NetExtender configurations don't have the executable, so
			// start off with the default path
			pPanel.setExecutable(Defaults.DEFAULT_EXE);
			saveConfigurations();
		}

		if (!pPanel.initializeExecutable(pPanel.getExecutable())) {
			// We need to select an executable else we can't move forward
			if (!pPanel.selectExecutableByDialog(null)) {
				JOptionPane.showMessageDialog(this.mainFrame, "Can't continue without the netExtender executable",
					"Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
		}

		cPanel.enableOrDisableConnectButton();
		tabbedPane.setSelectedIndex(0);
		initIndicator();
	}

	@Override
	public void run() {
		EventQueue.invokeLater(() -> {
			try {
				GUI gUI = new GUI();
				gUI.mainFrame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private boolean loadConfigurations() {
		final MainConfigurationFile configFile = new MainConfigurationFile();
		try {
			if (!configFile.loadFromDefault()) {
				// Nothing to be loaded...
				return false;
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this.mainFrame,
				"An exception was raised when loading the existing configurations, will start with a blank configuration",
				"Failed to load configurations", JOptionPane.INFORMATION_MESSAGE);
			this.log.error("Failed to load the configurations from the default location", e);
			return false;
		}

		Configuration configuration = new Configuration();
		configuration.load(configFile, MainConfigurationFile.Section.PROFILES,
			MainConfigurationFile.Section.PREFERENCES, MainConfigurationFile.Section.TRUSTEDCERTS);
		boolean loaded = false;
		for (Predicate<Configuration> p : this.loadFunctions) {
			loaded |= p.test(configuration);
		}
		return loaded;
	}

	private boolean saveConfigurations() {
		final MainConfigurationFile configFile = new MainConfigurationFile();
		Configuration configuration = new Configuration();

		this.saveFunctions.forEach((p) -> p.test(configuration));

		configuration.store(configFile, MainConfigurationFile.Section.PROFILES,
			MainConfigurationFile.Section.PREFERENCES, MainConfigurationFile.Section.TRUSTEDCERTS);
		try {
			configFile.storeToDefault();
			return true;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this.mainFrame,
				"An exception was raised when loading the existing configurations, will start with a blank configuration",
				"Failed to load configurations", JOptionPane.INFORMATION_MESSAGE);
			this.log.error("Failed to load the configurations from the default location", e);
		}
		return false;
	}

	private boolean importNetExtenderConfigurations() {
		final NetExtenderConfigurationFile configFile = new NetExtenderConfigurationFile();
		boolean loaded = false;
		try {
			try {
				configFile.loadFromDefault();
			} catch (IOException e) {
				this.log.warn("Failed to parse the existing NetExtender configurations", e);
				return false;
			}

			Configuration configuration = new Configuration();
			configuration.load(configFile, NetExtenderConfigurationFile.SECTION_PROFILES,
				NetExtenderConfigurationFile.SECTION_PREFERENCES, NetExtenderConfigurationFile.SECTION_TRUSTEDCERTS);

			if (JOptionPane.showConfirmDialog(this.mainFrame,
				"Would you like to import the existing NetExtender\nclient's profiles and preferences?\n\nNo passwords will be copied over.",
				"Import Profiles?", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
				for (Predicate<Configuration> p : this.loadFunctions) {
					loaded |= p.test(configuration);
				}
			}
		} catch (Exception e) {
			this.log.warn("Failed to read the existing NetExtender profiles and preferences", e);
		}

		return loaded;
	}

	private void initIndicator() {
		// TODO: Not yet implemented
	}
}
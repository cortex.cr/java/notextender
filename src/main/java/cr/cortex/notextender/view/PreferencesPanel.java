package cr.cortex.notextender.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.model.config.Configuration;
import cr.cortex.notextender.model.config.Defaults;
import cr.cortex.notextender.model.config.Preferences;
import cr.cortex.notextender.tools.ChildProcess;
import cr.cortex.notextender.tools.ChildProcess.LinePipedInput;

public class PreferencesPanel extends JPanel implements Preferences {
	private static final long serialVersionUID = 1L;

	private static class ExecutableInfo {
		private final File exe;
		private final String version;
		private final List<String> ciphers;

		private ExecutableInfo(File exe, String version, List<String> ciphers) {
			this.exe = exe;
			this.version = version;
			this.ciphers = ciphers;
		}
	}

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Runnable saveConfigurations;

	private JTextField executableField;
	private JButton executableBrowseButton;
	private JLabel clientVersionLabel;
	private JSpinner timeoutSpinner;
	private JComboBox<String> cipherCombo;
	private JCheckBox noRouteCheck;
	private JSpinner mtuSpinner;
	private JComboBox<PPPSynchronicity> pppSynchronicityCombo;
	private JComboBox<AutoReconnectMode> autoReconnectCombo;
	private JComboBox<DNSPreference> dnsPreferenceCombo;
	private JCheckBox savePasswordCheck;
	private JButton saveDiagnosticsButton;

	public PreferencesPanel(Runnable saveConfigurations) {
		this.saveConfigurations = (saveConfigurations != null ? saveConfigurations : UITools.NOOP);
		GridBagLayout gbl_optionsPanel = new GridBagLayout();
		setLayout(gbl_optionsPanel);

		Insets insets = new Insets(5, 5, 5, 5);

		JLabel lblExecutable = new JLabel("Executable Path");

		GridBagConstraints gbc_lblExecutable = new GridBagConstraints();
		gbc_lblExecutable.anchor = GridBagConstraints.EAST;
		gbc_lblExecutable.insets = insets;
		gbc_lblExecutable.gridx = 1;
		gbc_lblExecutable.gridy = 1;
		add(lblExecutable, gbc_lblExecutable);

		this.executableField = new JTextField();
		this.executableField.setToolTipText("This is the path where the NetExtender client executable is installed");
		lblExecutable.setLabelFor(this.executableField);
		this.executableField.setEditable(false);
		GridBagConstraints gbc_executableField = new GridBagConstraints();
		gbc_executableField.fill = GridBagConstraints.HORIZONTAL;
		gbc_executableField.insets = insets;
		gbc_executableField.gridx = 2;
		gbc_executableField.gridy = 1;
		add(this.executableField, gbc_executableField);
		this.executableField.setColumns(40);

		this.executableBrowseButton = new JButton("…");
		this.executableBrowseButton.setToolTipText("Click to browse the filesystem to find the executable");
		this.executableBrowseButton.setIcon(null);
		GridBagConstraints gbc_executableBrowseButton = new GridBagConstraints();
		gbc_executableBrowseButton.anchor = GridBagConstraints.WEST;
		gbc_executableBrowseButton.insets = insets;
		gbc_executableBrowseButton.gridx = 4;
		gbc_executableBrowseButton.gridy = 1;
		add(this.executableBrowseButton, gbc_executableBrowseButton);
		this.executableBrowseButton
			.addActionListener((event) -> selectExecutableByDialog(this.executableField.getText()));

		this.clientVersionLabel = new JLabel("");
		GridBagConstraints gbc_lblClientVersion = new GridBagConstraints();
		gbc_lblClientVersion.insets = insets;
		gbc_lblClientVersion.gridx = 2;
		gbc_lblClientVersion.gridy = 2;
		add(this.clientVersionLabel, gbc_lblClientVersion);

		JLabel lblTimeout = new JLabel("Connection Timeout");
		lblTimeout.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblTimeout = new GridBagConstraints();
		gbc_lblTimeout.anchor = GridBagConstraints.EAST;
		gbc_lblTimeout.insets = insets;
		gbc_lblTimeout.gridx = 1;
		gbc_lblTimeout.gridy = 3;
		add(lblTimeout, gbc_lblTimeout);

		this.timeoutSpinner = new JSpinner();
		this.timeoutSpinner.setModel(new SpinnerNumberModel(Defaults.DEFAULT_TIMEOUT, 1, 300, 1));
		this.timeoutSpinner.setToolTipText("Login timeout in seconds (default is 30 sec)");
		lblTimeout.setLabelFor(this.timeoutSpinner);
		GridBagConstraints gbc_timeoutSpinner = new GridBagConstraints();
		gbc_timeoutSpinner.anchor = GridBagConstraints.WEST;
		gbc_timeoutSpinner.insets = insets;
		gbc_timeoutSpinner.gridx = 2;
		gbc_timeoutSpinner.gridy = 3;
		add(this.timeoutSpinner, gbc_timeoutSpinner);

		JLabel lblCipher = new JLabel("Cipher");
		GridBagConstraints gbc_lblCipher = new GridBagConstraints();
		gbc_lblCipher.anchor = GridBagConstraints.EAST;
		gbc_lblCipher.insets = insets;
		gbc_lblCipher.gridx = 1;
		gbc_lblCipher.gridy = 4;
		add(lblCipher, gbc_lblCipher);

		this.cipherCombo = new JComboBox<>();
		this.cipherCombo.setToolTipText("Select the SSL cipher to use (leave blank to autodetect)");
		lblCipher.setLabelFor(this.cipherCombo);
		GridBagConstraints gbc_cipherCombo = new GridBagConstraints();
		gbc_cipherCombo.insets = insets;
		gbc_cipherCombo.fill = GridBagConstraints.HORIZONTAL;
		gbc_cipherCombo.gridx = 2;
		gbc_cipherCombo.gridy = 4;
		add(this.cipherCombo, gbc_cipherCombo);

		JLabel lblNoRoute = new JLabel("Disable Routing");
		GridBagConstraints gbc_lblNoRoute = new GridBagConstraints();
		gbc_lblNoRoute.anchor = GridBagConstraints.EAST;
		gbc_lblNoRoute.insets = insets;
		gbc_lblNoRoute.gridx = 1;
		gbc_lblNoRoute.gridy = 5;
		add(lblNoRoute, gbc_lblNoRoute);

		this.noRouteCheck = new JCheckBox("");
		this.noRouteCheck.setSelected(Defaults.DEFAULT_NO_ROUTE);
		this.noRouteCheck.setToolTipText(
			"Disable installation of remote routes. This is a testing option, and is not recommended for normal use!");
		lblNoRoute.setLabelFor(this.noRouteCheck);
		GridBagConstraints gbc_noRouteCheck = new GridBagConstraints();
		gbc_noRouteCheck.anchor = GridBagConstraints.WEST;
		gbc_noRouteCheck.insets = insets;
		gbc_noRouteCheck.gridx = 2;
		gbc_noRouteCheck.gridy = 5;
		add(this.noRouteCheck, gbc_noRouteCheck);

		JLabel lblMtu = new JLabel("MTU");
		GridBagConstraints gbc_lblMtu = new GridBagConstraints();
		gbc_lblMtu.anchor = GridBagConstraints.EAST;
		gbc_lblMtu.insets = insets;
		gbc_lblMtu.gridx = 1;
		gbc_lblMtu.gridy = 6;
		add(lblMtu, gbc_lblMtu);

		this.mtuSpinner = new JSpinner();
		lblMtu.setLabelFor(this.mtuSpinner);
		this.mtuSpinner.setModel(new SpinnerNumberModel(Defaults.DEFAULT_MTU, 128, 1500, 1));
		this.mtuSpinner.setToolTipText("Specify MTU size in bytes (the default is 1,280)");
		GridBagConstraints gbc_mtuSpinner = new GridBagConstraints();
		gbc_mtuSpinner.anchor = GridBagConstraints.WEST;
		gbc_mtuSpinner.insets = insets;
		gbc_mtuSpinner.gridx = 2;
		gbc_mtuSpinner.gridy = 6;
		add(this.mtuSpinner, gbc_mtuSpinner);

		JLabel lblPppSynchronicity = new JLabel("PPP Synchronicity");
		GridBagConstraints gbc_lblPppSynchronicity = new GridBagConstraints();
		gbc_lblPppSynchronicity.anchor = GridBagConstraints.EAST;
		gbc_lblPppSynchronicity.insets = insets;
		gbc_lblPppSynchronicity.gridx = 1;
		gbc_lblPppSynchronicity.gridy = 7;
		add(lblPppSynchronicity, gbc_lblPppSynchronicity);

		this.pppSynchronicityCombo = new JComboBox<>();
		lblPppSynchronicity.setLabelFor(this.pppSynchronicityCombo);
		this.pppSynchronicityCombo.setToolTipText("Select the PPP synchronicity mode");
		this.pppSynchronicityCombo.setModel(new DefaultComboBoxModel<>(PPPSynchronicity.values()));
		this.pppSynchronicityCombo.setSelectedItem(Defaults.DEFAULT_PPP_SYNCHRONICITY);
		GridBagConstraints gbc_pppSynchronicityCombo = new GridBagConstraints();
		gbc_pppSynchronicityCombo.anchor = GridBagConstraints.WEST;
		gbc_pppSynchronicityCombo.insets = insets;
		gbc_pppSynchronicityCombo.gridx = 2;
		gbc_pppSynchronicityCombo.gridy = 7;
		add(this.pppSynchronicityCombo, gbc_pppSynchronicityCombo);

		JLabel lblAutoReconnect = new JLabel("Automatic Reconnection");
		GridBagConstraints gbc_lblAutoReconnect = new GridBagConstraints();
		gbc_lblAutoReconnect.anchor = GridBagConstraints.EAST;
		gbc_lblAutoReconnect.insets = insets;
		gbc_lblAutoReconnect.gridx = 1;
		gbc_lblAutoReconnect.gridy = 8;
		add(lblAutoReconnect, gbc_lblAutoReconnect);

		this.autoReconnectCombo = new JComboBox<>();
		this.autoReconnectCombo.setModel(new DefaultComboBoxModel<>(AutoReconnectMode.values()));
		this.autoReconnectCombo.setSelectedItem(Defaults.DEFAULT_AUTO_RECONNECT);
		lblAutoReconnect.setLabelFor(this.autoReconnectCombo);
		this.autoReconnectCombo.setToolTipText("Select the reconnection mode to be used");
		GridBagConstraints gbc_autoReconnectCombo = new GridBagConstraints();
		gbc_autoReconnectCombo.anchor = GridBagConstraints.WEST;
		gbc_autoReconnectCombo.insets = insets;
		gbc_autoReconnectCombo.gridx = 2;
		gbc_autoReconnectCombo.gridy = 8;
		add(this.autoReconnectCombo, gbc_autoReconnectCombo);

		JLabel lblDnsPreference = new JLabel("DNS Preference");
		GridBagConstraints gbc_lblDnsPreference = new GridBagConstraints();
		gbc_lblDnsPreference.anchor = GridBagConstraints.EAST;
		gbc_lblDnsPreference.insets = insets;
		gbc_lblDnsPreference.gridx = 1;
		gbc_lblDnsPreference.gridy = 9;
		add(lblDnsPreference, gbc_lblDnsPreference);

		this.dnsPreferenceCombo = new JComboBox<>();
		this.dnsPreferenceCombo.setModel(new DefaultComboBoxModel<>(DNSPreference.values()));
		this.dnsPreferenceCombo.setSelectedItem(Defaults.DEFAULT_DNS_PREFERENCE);
		GridBagConstraints gbc_dnsPreferenceCombo = new GridBagConstraints();
		gbc_dnsPreferenceCombo.anchor = GridBagConstraints.WEST;
		gbc_dnsPreferenceCombo.insets = insets;
		gbc_dnsPreferenceCombo.gridx = 2;
		gbc_dnsPreferenceCombo.gridy = 9;
		add(this.dnsPreferenceCombo, gbc_dnsPreferenceCombo);

		JLabel lblSavePasswords = new JLabel("Save Passwords");
		GridBagConstraints gbc_lblSavePasswords = new GridBagConstraints();
		gbc_lblSavePasswords.anchor = GridBagConstraints.EAST;
		gbc_lblSavePasswords.insets = insets;
		gbc_lblSavePasswords.gridx = 1;
		gbc_lblSavePasswords.gridy = 10;
		add(lblSavePasswords, gbc_lblSavePasswords);

		this.savePasswordCheck = new JCheckBox("");
		this.savePasswordCheck.setSelected(Defaults.DEFAULT_SAVE_PASSWORDS);
		this.savePasswordCheck
			.setToolTipText("Enable or disable saving the (encoded) passwords to the configuration file");
		lblSavePasswords.setLabelFor(this.savePasswordCheck);
		GridBagConstraints gbc_savePasswordCheck = new GridBagConstraints();
		gbc_savePasswordCheck.insets = insets;
		gbc_savePasswordCheck.anchor = GridBagConstraints.WEST;
		gbc_savePasswordCheck.gridx = 2;
		gbc_savePasswordCheck.gridy = 10;
		add(this.savePasswordCheck, gbc_savePasswordCheck);

		JLabel lblSaveDiagnostics = new JLabel("Save Diagnostics");
		GridBagConstraints gbc_lblSaveDiagnostics = new GridBagConstraints();
		gbc_lblSaveDiagnostics.anchor = GridBagConstraints.EAST;
		gbc_lblSaveDiagnostics.insets = insets;
		gbc_lblSaveDiagnostics.gridx = 1;
		gbc_lblSaveDiagnostics.gridy = 11;
		add(lblSaveDiagnostics, gbc_lblSaveDiagnostics);

		this.saveDiagnosticsButton = new JButton("");
		this.saveDiagnosticsButton.setEnabled(false);
		this.saveDiagnosticsButton.addActionListener((event) -> {
			JFileChooser chooser = new JFileChooser(".");
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setDialogTitle("Select where to save the diagnostics");
			while (true) {
				switch (chooser.showDialog(null, "Save")) {
					case JFileChooser.APPROVE_OPTION:
						File target = chooser.getSelectedFile();
						try {
							target = target.getCanonicalFile();
							if (target.exists()) {
								if (JOptionPane.showConfirmDialog(this,
									String.format("Overwrite the selected file?\n\n[%s]", target.getAbsolutePath()),
									"Confirm", JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION) {
									continue;
								}
							}
							saveDiagnostics(target);
							JOptionPane.showMessageDialog(this,
								String.format("Diagnostics were saved into [%s]", target.getAbsolutePath()),
								"Diagnostics Saved", JOptionPane.INFORMATION_MESSAGE);
						} catch (Exception e) {
							this.log.error("Exception caught while trying to save diagnostics to [{}]", target, e);
							JOptionPane.showMessageDialog(this,
								String.format(
									"Failed to save the diagnostics to the file [%s]\n\n%s\n\nPlease try again", target,
									e.getMessage()),
								"Error", JOptionPane.ERROR_MESSAGE);
							continue;
						}
						// fall-through

					default:
						return;
				}
			}
		});
		lblSaveDiagnostics.setLabelFor(this.saveDiagnosticsButton);
		this.saveDiagnosticsButton.setToolTipText("Generate a diagnostic report");
		this.saveDiagnosticsButton.setIcon(new ImageIcon(getClass().getResource("/img/diagnostics.png")));
		GridBagConstraints gbc_saveDiagnosticsButton = new GridBagConstraints();
		gbc_saveDiagnosticsButton.anchor = GridBagConstraints.WEST;
		gbc_saveDiagnosticsButton.insets = insets;
		gbc_saveDiagnosticsButton.gridx = 2;
		gbc_saveDiagnosticsButton.gridy = 11;
		add(this.saveDiagnosticsButton, gbc_saveDiagnosticsButton);

		this.executableField.getDocument().addDocumentListener(new DocumentListener() {
			private void processUpdate(DocumentEvent e) {
				final Document doc = e.getDocument();
				try {
					PreferencesPanel.this.saveDiagnosticsButton
						.setEnabled(validateExecutable(doc.getText(0, doc.getLength())));
				} catch (BadLocationException ex) {
					PreferencesPanel.this.log.error("Failed to process a field change", ex);
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				processUpdate(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				processUpdate(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				processUpdate(e);
			}
		});

		addComponentListener(new ComponentAdapter() {

			private boolean shown = false;

			@Override
			public void componentShown(ComponentEvent e) {
				this.shown = true;
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				if (this.shown) {
					PreferencesPanel.this.saveConfigurations.run();
				}
			}
		});
	}

	boolean selectExecutableByDialog(String startingPoint) {
		ExecutableInfo info = chooseExecutable(startingPoint);
		if (info == null) { return false; }
		populateFromExecutable(info);
		return true;
	}

	private boolean validateExecutable(String exe) {
		if (StringUtils.isBlank(exe)) { return false; }
		File f = new File(exe);
		if (!f.exists()) { return false; }
		if (!f.isFile()) { return false; }
		if (!f.canRead()) { return false; }
		if (!f.canExecute()) { return false; }
		return true;
	}

	public boolean initializeExecutable(String configured) {
		if (configured == null) {
			configured = Defaults.DEFAULT_EXE;
		}
		// Try the default executable
		File exe = new File(configured);
		if (exe.exists() && exe.canRead() && exe.canExecute()) {
			try {
				populateFromExecutable(
					new ExecutableInfo(exe.getCanonicalFile(), getClientVersion(exe), getCiphers(exe)));
				return true;
			} catch (Exception e) {
				// Ignore it...
			}
		}
		return false;
	}

	private ExecutableInfo chooseExecutable(String start) {
		JFileChooser chooser = new JFileChooser(new File("."));
		if (validateExecutable(start)) {
			File current = new File(start);
			chooser.setSelectedFile(current);
			chooser.setCurrentDirectory(current.getParentFile());
		}
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				if (f == null) { return false; }
				if (f.isDirectory()) { return true; }
				return (f.isFile() && f.canRead() && f.canExecute());
			}

			@Override
			public String getDescription() {
				return "Executables";
			}
		});

		chooser.setDialogTitle("Find the NetExtender CLI executable");
		while (true) {
			switch (chooser.showDialog(null, "Select")) {
				case JFileChooser.APPROVE_OPTION:
					try {
						File executable = chooser.getSelectedFile().getCanonicalFile();
						// Ok so this should be a regular, executable file...is it what we want?
						String version = getClientVersion(executable);
						if (version == null) {
							// Show an error message, and try again
							JOptionPane
								.showMessageDialog(this,
									String.format("The executable at [%s] doesn't appear to be the netExtender client",
										executable.getCanonicalPath()),
									"Invalid Executable", JOptionPane.ERROR_MESSAGE);
							continue;
						} else {
							int opt = JOptionPane.showConfirmDialog(this, String.format(
								"The executable at [%s]\nappears to be the NetExtender client v%s\n\nDo you want to use this executable?",
								executable.getCanonicalPath(), version), "Executable found!", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
							if (opt == JOptionPane.NO_OPTION) {
								continue;
							}
						}

						List<String> ciphers = null;
						try {
							ciphers = Collections.unmodifiableList(getCiphers(executable));
						} catch (Exception e) {
							this.log.error("Failed to get the cipher list from [{}]", executable, e);
							JOptionPane.showMessageDialog(this, String
								.format("Failed to get the list of ciphers from the executable at [%s]", executable),
								"Executable Error", JOptionPane.ERROR_MESSAGE);
							continue;
						}

						return new ExecutableInfo(executable, version, ciphers);
					} catch (Exception e) {
						this.log.error("Unchecked exception caught while selecting the executable", e);
						JOptionPane.showMessageDialog(this,
							String.format(
								"An unchecked exception was caught, please check the error log for details\n\n%s",
								e.getClass().getCanonicalName()),
							"Uncaught Exception", JOptionPane.ERROR_MESSAGE);
						continue;
					}

				case JFileChooser.ERROR_OPTION:
				case JFileChooser.CANCEL_OPTION:
					return null;
			}
		}
	}

	private String getClientVersion(File executable) throws Exception {
		String[] cmd = {
			executable.getCanonicalPath(), "-V"
		};

		ChildProcess cp = null;
		List<String> out = Collections.synchronizedList(new LinkedList<>());

		cp = new ChildProcess(executable.getParentFile(), cmd);
		LinePipedInput output = new LinePipedInput((l, s) -> out.add(s));
		cp.start(null, null, output, output).join(30, TimeUnit.SECONDS);

		cmd[1] = "-v";

		cp = new ChildProcess(executable.getParentFile(), cmd);
		cp.start(null, null, output, output).join(30, TimeUnit.SECONDS);

		// Ok so the first line is the raw version number, and the rest is the other stuff...
		// The version number should show up at the end of the second line
		if (out.size() < 2) { return null; }
		String version = out.get(0);
		if (version == null) { return null; }
		String header = out.get(1);
		if (header == null) { return null; }
		if (!header.matches(".* " + version)) { return null; }

		return version;
	}

	private void saveDiagnostics(File target) throws Exception {
		final File parent = target.getParentFile();
		if (!parent.exists() && !parent.mkdirs()) {
			throw new IOException(String.format("Failed to create the directory tree [%s]", parent.getAbsolutePath()));
		}

		final File temp = File.createTempFile("netextender.diagnostics.", null, parent).getCanonicalFile();
		final File executable = new File(this.executableField.getText());
		String[] cmd = {
			executable.getCanonicalPath(), "-r", temp.getAbsolutePath()
		};

		ChildProcess cp = null;
		cp = new ChildProcess(executable.getParentFile(), cmd);
		cp.start(null, null, null, null).join(30, TimeUnit.SECONDS);

		if (!temp.exists()) { throw new IOException(String.format("The diagnostics were not saved to [%s]", target)); }
		if (!temp.renameTo(target)) {
			throw new IOException(String.format("Failed to move the diagnostics from [%s] to [%s]", temp, target));
		}
	}

	private List<String> getCiphers(File executable) throws Exception {
		String[] cmd = {
			executable.getCanonicalPath(), "--cipher-list"
		};

		ChildProcess cp = null;
		List<String> out = new LinkedList<>();
		List<String> sync = Collections.synchronizedList(out);

		cp = new ChildProcess(executable.getParentFile(), cmd);
		LinePipedInput output = new LinePipedInput((l, s) -> sync.add(s));
		cp.start(null, null, output, output).join(30, TimeUnit.SECONDS);

		if (!out.isEmpty()) {
			out.replaceAll(String::toUpperCase);
			out.replaceAll(String::trim);
			out.removeIf((s) -> s.lastIndexOf(':') > 0);
			out.removeIf(StringUtils::isBlank);
			Collections.sort(out);
			out.add(0, Defaults.DEFAULT_CIPHER);
		}
		return out;
	}

	private void populateFromExecutable(ExecutableInfo info) {
		this.executableField.setText(info.exe.getAbsolutePath());
		this.clientVersionLabel.setText("Client Version: " + info.version);
		this.cipherCombo.setModel(new DefaultComboBoxModel<>(new Vector<>(info.ciphers)));
	}

	private boolean copyData(Stream<Pair<Supplier<Object>, Consumer<Object>>> s) {
		final AtomicBoolean changed = new AtomicBoolean(false);
		s.forEach((p) -> {
			Object v = p.getKey().get();
			if (v != null) {
				p.getValue().accept(v);
			}
			changed.set(true);
		});
		return changed.get();
	}

	boolean loadPreferences(Configuration cfg) {
		Stream<Pair<Supplier<Object>, Consumer<Object>>> s = Stream.of( //
			Pair.of(cfg::getExecutable, (v) -> this.executableField.setText(v.toString())), //
			Pair.of(cfg::getTimeout, this.timeoutSpinner::setValue), //
			Pair.of(cfg::getCipher, this.cipherCombo::setSelectedItem), //
			Pair.of(cfg::getNoRoute, (v) -> this.noRouteCheck.setSelected(Boolean.class.cast(v))), //
			Pair.of(cfg::getAutoReconnect, this.autoReconnectCombo::setSelectedItem), //
			Pair.of(cfg::getMtu, this.mtuSpinner::setValue), //
			Pair.of(cfg::getPppSynchronicity, this.pppSynchronicityCombo::setSelectedItem),
			Pair.of(cfg::getDnsPreference, this.dnsPreferenceCombo::setSelectedItem), //
			Pair.of(cfg::isSavePasswords, (v) -> this.savePasswordCheck.setSelected(Boolean.class.cast(v)))
		//
		);
		boolean changed = copyData(s);
		return changed;
	}

	boolean savePreferences(Configuration cfg) {
		Stream<Pair<Supplier<Object>, Consumer<Object>>> s = Stream.of( //
			Pair.of(this.executableField::getText, (v) -> cfg.setExecutable(v.toString())), //
			Pair.of(this.timeoutSpinner::getValue, (v) -> cfg.setTimeout(Integer.class.cast(v))), //
			Pair.of(this.cipherCombo::getSelectedItem, (v) -> cfg.setCipher(v.toString())), //
			Pair.of(this.noRouteCheck::isSelected, (v) -> cfg.setNoRoute(Boolean.class.cast(v))), //
			Pair.of(this.autoReconnectCombo::getSelectedItem,
				(v) -> cfg.setAutoReconnect(AutoReconnectMode.class.cast(v))), //
			Pair.of(this.mtuSpinner::getValue, (v) -> cfg.setMtu(Integer.class.cast(v))), //
			Pair.of(this.pppSynchronicityCombo::getSelectedItem,
				(v) -> cfg.setPppSynchronicity(PPPSynchronicity.class.cast(v))),
			Pair.of(this.dnsPreferenceCombo::getSelectedItem,
				(v) -> cfg.setDnsPreferenceMode(DNSPreference.class.cast(v))), //
			Pair.of(this.savePasswordCheck::isSelected, (v) -> cfg.setSavePasswords(Boolean.class.cast(v))) //
		);
		boolean changed = copyData(s);
		return changed;
	}

	@Override
	public String getExecutable() {
		return this.executableField.getText();
	}

	@Override
	public Preferences setExecutable(String executable) {
		this.executableField.setText(getOrDefault(executable, Defaults.DEFAULT_EXE));
		return this;
	}

	@Override
	public Integer getTimeout() {
		return Integer.class.cast(this.timeoutSpinner.getValue());
	}

	@Override
	public Preferences setTimeout(Integer timeout) {
		this.timeoutSpinner.setValue(getOrDefault(timeout, Defaults.DEFAULT_TIMEOUT));
		return this;
	}

	@Override
	public String getCipher() {
		return this.cipherCombo.getSelectedItem().toString();
	}

	@Override
	public Preferences setCipher(String cipher) {
		this.cipherCombo.setSelectedItem(getOrDefault(cipher, Defaults.DEFAULT_CIPHER));
		return this;
	}

	@Override
	public Boolean getNoRoute() {
		return this.noRouteCheck.isSelected();
	}

	@Override
	public Preferences setNoRoute(Boolean noRoute) {
		this.noRouteCheck.setSelected(getOrDefault(noRoute, Defaults.DEFAULT_NO_ROUTE));
		return this;
	}

	@Override
	public Integer getMtu() {
		return Integer.class.cast(this.mtuSpinner.getValue());
	}

	@Override
	public Preferences setMtu(Integer mtu) {
		this.mtuSpinner.setValue(getOrDefault(mtu, Defaults.DEFAULT_MTU));
		return this;
	}

	@Override
	public PPPSynchronicity getPppSynchronicity() {
		return PPPSynchronicity.class.cast(this.pppSynchronicityCombo.getSelectedItem());
	}

	@Override
	public Preferences setPppSynchronicity(PPPSynchronicity pppSynchronicity) {
		this.pppSynchronicityCombo.setSelectedItem(getOrDefault(pppSynchronicity, Defaults.DEFAULT_PPP_SYNCHRONICITY));
		return this;
	}

	@Override
	public AutoReconnectMode getAutoReconnect() {
		return AutoReconnectMode.class.cast(this.autoReconnectCombo.getSelectedItem());
	}

	@Override
	public Preferences setAutoReconnect(AutoReconnectMode autoReconnect) {
		this.autoReconnectCombo.setSelectedItem(getOrDefault(autoReconnect, Defaults.DEFAULT_AUTO_RECONNECT));
		return this;
	}

	@Override
	public DNSPreference getDnsPreference() {
		return DNSPreference.class.cast(this.dnsPreferenceCombo.getSelectedItem());
	}

	@Override
	public Preferences setDnsPreferenceMode(DNSPreference dnsPreference) {
		this.dnsPreferenceCombo.setSelectedItem(getOrDefault(dnsPreference, Defaults.DEFAULT_DNS_PREFERENCE));
		return this;
	}

	@Override
	public Boolean isSavePasswords() {
		return this.savePasswordCheck.isSelected();
	}

	@Override
	public Preferences setSavePasswords(Boolean savePasswords) {
		this.savePasswordCheck.setSelected(getOrDefault(savePasswords, Defaults.DEFAULT_SAVE_PASSWORDS));
		return this;
	}
}
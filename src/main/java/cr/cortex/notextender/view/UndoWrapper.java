package cr.cortex.notextender.view;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Objects;
import java.util.function.Supplier;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

public class UndoWrapper<COMPONENT extends JTextComponent> implements Supplier<COMPONENT> {

	protected class UndoHandler implements UndoableEditListener {
		@Override
		public void undoableEditHappened(UndoableEditEvent e) {
			UndoWrapper.this.undoManager.addEdit(e.getEdit());
			UndoWrapper.this.undoAction.update();
			UndoWrapper.this.redoAction.update();
		}
	}

	protected class UndoAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public UndoAction() {
			super("Undo");
			setEnabled(false);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				UndoWrapper.this.undoManager.undo();
			} catch (CannotUndoException ex) {
				// TODO deal with this
				ex.printStackTrace();
			}
			update();
			UndoWrapper.this.redoAction.update();
		}

		protected void update() {
			if (UndoWrapper.this.undoManager.canUndo()) {
				setEnabled(true);
				putValue(Action.NAME, UndoWrapper.this.undoManager.getUndoPresentationName());
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Undo");
			}
		}
	}

	protected class RedoAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public RedoAction() {
			super("Redo");
			setEnabled(false);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				UndoWrapper.this.undoManager.redo();
			} catch (CannotRedoException ex) {
				// TODO deal with this
				ex.printStackTrace();
			}
			update();
			UndoWrapper.this.undoAction.update();
		}

		protected void update() {
			if (UndoWrapper.this.undoManager.canRedo()) {
				setEnabled(true);
				putValue(Action.NAME, UndoWrapper.this.undoManager.getRedoPresentationName());
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Redo");
			}
		}
	}

	private final UndoManager undoManager = new UndoManager();
	private final UndoHandler undoHandler = new UndoHandler();
	private final UndoAction undoAction = new UndoAction();
	private final RedoAction redoAction = new RedoAction();

	private final COMPONENT component;

	public UndoWrapper(COMPONENT component) {
		this.component = Objects.requireNonNull(component, "Must provide a JTextComponent to attach into");

		KeyStroke undoKeystroke = KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK);
		KeyStroke redoKeystroke = KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK);

		component.getInputMap().put(undoKeystroke, "undoKeystroke");
		component.getActionMap().put("undoKeystroke", this.undoAction);

		component.getInputMap().put(redoKeystroke, "redoKeystroke");
		component.getActionMap().put("redoKeystroke", this.redoAction);

		this.component.getDocument().addUndoableEditListener(this.undoHandler);
	}

	public UndoManager getUndoManager() {
		return this.undoManager;
	}

	@Override
	public COMPONENT get() {
		return this.component;
	}
}
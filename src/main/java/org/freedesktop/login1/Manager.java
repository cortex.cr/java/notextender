package org.freedesktop.login1;

import org.freedesktop.dbus.FileDescriptor;
import org.freedesktop.dbus.annotations.DBusInterfaceName;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.messages.DBusSignal;

@DBusInterfaceName("org.freedesktop.login1.Manager")
public interface Manager extends DBusInterface {

	public FileDescriptor Inhibit(String what, String who, String why, String mode);

	public class PrepareForSleep extends DBusSignal {
		private final boolean starting;

		public PrepareForSleep(String _objectPath, boolean starting) throws DBusException {
			super(_objectPath, starting);
			this.starting = starting;
		}

		public PrepareForSleep(String _source, String _path, String _iface, String _member, String _sig,
			boolean starting) throws DBusException {
			super(_source, _path, _iface, _member, _sig, starting);
			this.starting = starting;
		}

		public final boolean isStarting() {
			return this.starting;
		}
	}
}
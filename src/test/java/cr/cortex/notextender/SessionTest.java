package cr.cortex.notextender;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cr.cortex.notextender.DBusAdapter.InhibitorLock;

public class SessionTest {

	private final Logger log = LoggerFactory.getLogger(getClass());

	// @Test
	public void testSession() throws Exception {
		URI server = new URI("https", null, "access.armedia.com", 4433, null, null, null);
		Session s = new Session(server, "ARMEDIA", "drivera", "!N3v3rm0r3@11!"::toCharArray, null);

		try {
			s.login(null);
			s.run(this.log::debug);
		} finally {
			s.logout();
		}
	}

	// @Test
	public void testInhibitor() throws Exception {

		try (InhibitorLock lock = DBusAdapter.delaySuspendAndShutdown("This is a test")) {

			this.log.debug("{}", lock);
		}
	}

}
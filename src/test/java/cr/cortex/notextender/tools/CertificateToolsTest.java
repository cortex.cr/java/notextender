package cr.cortex.notextender.tools;

import java.security.cert.X509Certificate;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CertificateToolsTest {

	@Test
	public void testGetCertificatesInetAddressInt() throws Exception {
	}

	@Test
	public void testGetCertificatesStringInt() throws Exception {
		Pair<Boolean, List<X509Certificate>> certs = CertificateTools.getCertificates("www.google.com", 443);
		Assertions.assertNotNull(certs);
		Assertions.assertNotNull(certs.getKey());
		Assertions.assertNotNull(certs.getValue());
		Assertions.assertTrue(certs.getKey());
	}

}